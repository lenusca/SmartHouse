# -*- coding: utf-8 -*-
import sys
import time as Time
from random import randint
import json
from datetime import datetime
from kafka import KafkaProducer

######################################################################################################## CONSTANTES ##########################################################################################

#['aparelho', gastoPorHoraWatts, %tempoLigado]
COZINHA = [['Fridge', 100, 1], ['Coffee Machine', 500, 0.2], ['Microwave', 300, 0.2]]
SALA = [['TV', 120, 0.4], ['Playstation', 146.4, 0.05], ['Radio', 45, 0.05]]
QUARTO = [['TV', 120, 0.1], ['PC', 40,  0.5], ['Heater', 1500, 0.05]]
WC = [['Eletric Toothbrush', 50, 0.04], ['Hair Dryer', 1400, 0.02], ['Dehumidifier', 200, 1]]

costDivisao = []


def main(argv):


    while True:
        gastoNaUltimaHoraK = []
        gastoNaUltimaHoraS = []
        gastoNaUltimaHoraR = []
        gastoNaUltimaHoraWC = []

        for aparelho in COZINHA:
            gastoNaUltimaHoraK.append([aparelho[0], 0])

        for aparelho in SALA:
            gastoNaUltimaHoraS.append([aparelho[0], 0])

        for aparelho in QUARTO:
            gastoNaUltimaHoraR.append([aparelho[0], 0])

        for aparelho in WC:
            gastoNaUltimaHoraWC.append([aparelho[0], 0])

        gastoNaUltimaHoraK = calcEnergia(gastoNaUltimaHoraK, COZINHA)
        gastoNaUltimaHoraS = calcEnergia(gastoNaUltimaHoraS, SALA)
        gastoNaUltimaHoraR = calcEnergia(gastoNaUltimaHoraR, QUARTO)
        gastoNaUltimaHoraWC = calcEnergia(gastoNaUltimaHoraWC, WC)

        show('cozinha', gastoNaUltimaHoraK)
        show('sala', gastoNaUltimaHoraS)
        show('quarto', gastoNaUltimaHoraR)
        show('wc', gastoNaUltimaHoraWC)

        writeToJson()

        costDivisao.clear()

        Time.sleep(0.1 * 15)

def calcEnergia(gastoNaUltimaHora, aparelhos):
    i = 0
    rand = (randint(0, 100))
    for a,b in gastoNaUltimaHora:

        # DIA
        if rand < 50:
            b = round(aparelhos[i][1] * aparelhos[i][2] * 0.14810, 4)
        # NOITE
        else:
            b = round(aparelhos[i][1] * aparelhos[i][2] * 0.072405, 4)

        gastoNaUltimaHora[i] = [aparelhos[i][0], b]

        i += 1

    return gastoNaUltimaHora

def show(id, gastoNaUltimaHora):
    time = str(datetime.now())
    cost1 = {'name':gastoNaUltimaHora[0][0], 'value': gastoNaUltimaHora[0][1], 'time': time}
    cost2 = {'name':gastoNaUltimaHora[1][0], 'value': gastoNaUltimaHora[1][1], 'time': time}
    cost3 = {'name':gastoNaUltimaHora[2][0], 'value': gastoNaUltimaHora[2][1], 'time': time}
    print(len(costDivisao))
    costDivisao.append({'idDivisao': id, 'd1': cost1, 'd2': cost2, 'd3': cost3})

################################################################################################ESCREVER EM JSON##############################################################################################
def writeToJson():
    #file = open('cost.json', 'wb')
    producer = KafkaProducer(bootstrap_servers="192.168.160.103:9092")
    data_string = json.dumps(costDivisao)
    producer.send('smarthouse-cost', value=data_string.encode('utf-8'))
    Time.sleep(1)

if __name__ == "__main__":
    main(sys.argv)
