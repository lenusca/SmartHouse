import random
import time as Time
import sys
import json
from datetime import datetime
from kafka import KafkaProducer
from kafka import KafkaConsumer
import smtplib, ssl
##################### UNIDADES ######################
# temperatura       ºC
# CO2               ppm
# humidade          %
# movimento         YES/NO
# luzes             ON/OFF
#######################################################
####################### DADOS #########################
DIVISOES = ['cozinha', 'sala', 'quarto', 'wc']
#####TEMPERATURA#####
T_MIN = 10
T_MAX = 30
T_INICIAL = 20
T_ASKED = [None, None, None, None]
#########CO2#########
CO2_MIN = 100
CO2_MAX = 500
CO2_INICIAL = 320
CO2_ASKED = [None, None, None, None]
######HUMIDADE######
H_MIN = 20
H_MAX = 90
H_INICIAL = 45
H_ASKED = [None, None, None, None]
####LUMINOSIDADE####
L_ON = True
L_OFF = False
L_INICIAL = False
L_ASKED = [None, None, None, None]
######MOVIMENTO######
M_TEMPO = 50
M_ON = True
M_OFF = False
M_INICIAL = False
consumer = KafkaConsumer(bootstrap_servers="192.168.160.103:9092",value_deserializer=lambda m: m.decode('utf-8'))
#######SENDING EMAIL#####
port = 465 # SSL
password = "es20192020"
received_Kafka = ""
def main(argv):
    consumer.subscribe(['smarthouse-filtered'])
    temperatura = []
    co2 = []
    humidade = []
    luminosidade = []
    movimento = []
    # QUANTOS RANDOMS TEM DE CRIAR
    for x in range(0, len(DIVISOES)):
        temperatura.append(T_INICIAL)
        co2.append(CO2_INICIAL)
        humidade.append(H_INICIAL)
        luminosidade.append(L_INICIAL)
        movimento.append(M_INICIAL)
    # ESTAR SEMPRE A CORRER
    while True:
        msg = consumer.poll(5)
        for tp, messages in msg.items():
            for message in messages:
                received_Kafka = message.value
                print (received_Kafka)
                if received_Kafka != "":
                    changevalues(received_Kafka)
        temperatura = randomTemperatura(temperatura)
        co2 = randomCO2(co2)
        humidade = randomHumidade(humidade)
        luminosidade = randomLuminosidade(luminosidade)
        movimento = randomMovimento(movimento)
        show(temperatura, co2, humidade, luminosidade, movimento)
        #print("Stuck----!")
        ##for msg in consumer:
        ##    print("Stuck!!!!!")
        ##    print(msg.value)
        ##print("Stuckajasjd!")
        Time.sleep(0.1 * 15)
def randomTemperatura(lista):
    i = 0
    for x in lista:
        rand = (random.randint(-2, 2))
        if x + rand < T_MIN:
            lista[i] = T_MIN
        elif x + rand > T_MAX:
            lista[i] = T_MAX
        else:
            lista[i] = x + rand
        i += 1
    return lista
def randomCO2(lista):
    i = 0    
    for x in lista:
        rand = (random.randint(-2, 2))
        if x + rand < CO2_MIN:
            lista[i] = CO2_MIN
        elif x + rand > CO2_MAX:
            lista[i] = CO2_MAX
        else:
            lista[i] = x + rand
        i += 1
    return lista
def randomHumidade(lista):
    i = 0
    for x in lista:
        rand = (random.randint(-2, 2))
        if x + rand < H_MIN:
            lista[i] = H_MIN
        elif x + rand > H_MAX:
            lista[i] = H_MAX
        else:
            lista[i] = x + rand
        i += 1
    return lista
def randomLuminosidade(lista):
    i = 0
    for x in lista:
        rand = random.choice([True, False])
        lista[i] = rand
        i += 1
    return lista
def randomMovimento(lista):
    i = 0
    for x in lista:
        rand = random.randint(0, 100)
        if rand < M_TEMPO:
            lista[i] = True
        else:
            lista[i] = False
        i += 1
    return lista
def show(temperatura, co2, humidade, luminosidade, movimento):
    j= 0
    dataDivisao = []
    time = str(datetime.now())
    producer = KafkaProducer(bootstrap_servers="192.168.160.103:9092")
    for x in DIVISOES:
        if(T_ASKED[j] != None):
            dataluminosidade= {'value': T_ASKED[j], 'time': time}
            # print(T_ASKED)
            # print("\n\n")
        else:
            dataluminosidade= {'value': temperatura[j], 'time': time}
        if(CO2_ASKED[j] != None):
            dataco2 = {'value': CO2_ASKED[j], 'time': time}
            # print(CO2_ASKED)
            # print("\n\n"
        else:
            dataco2 = {'value': co2[j], 'time': time}
        if(H_ASKED[j] != None):
            datahumidade = {'value': H_ASKED[j], 'time': time}
            # print(H_ASKED)
            # print("\n\n")
        else:
            datahumidade = {'value': humidade[j], 'time': time}
        if(L_ASKED[j] != None):
            datalight = {'value': L_ASKED[j], 'time': time}
            # print(L_ASKED)
            # print("\n\n")
        else:
            datalight = {'value': luminosidade[j], 'time': time}
        datamovement = {'value': movimento[j], 'time': time}
        # if movimento[j] == True:
        #     sendingMail(x)
        dataDivisao.append({'idDivision': x, 'time': time,'temperature': dataluminosidade, 'co2': dataco2, 'humidity': datahumidade, 'light': datalight, 'movement': datamovement })
        # elastic search
        producer2 = KafkaProducer(bootstrap_servers="192.168.160.103:9093")
        data_string = json.dumps({'idDivisao':x, 'temperatura': temperatura[j], 'co2': co2[j], 'humidade': humidade[j], 'luz': luminosidade[j], 'movimento': movimento[j]})
        producer2.send('smarthouse-logstach', value=data_string.encode('utf-8'))
        j += 1
    data_string = json.dumps(dataDivisao)
    producer.send('smarthouse-topic', value=data_string.encode('utf-8'))
    Time.sleep(1)
    Time.sleep(1)
def sendingMail(divisao):
    # ligação segura entre Gmail e SMTP server
    context = ssl.create_default_context()
     # enviar o mail, um plain-text
    sender_email = "smarthouse20192020@gmail.com"
    receiver_email = "helenamncardoso@gmail.com"
    message = """\
        Subject: Movement Detection

        Is someone in the division """ +divisao
    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login("smarthouse20192020@gmail.com", password)
        server.sendmail(sender_email, receiver_email, message)
def changevalues(message):
    # se tem divisão e se tiver só tem de mudar o valor especifico da divisão
    global T_ASKED
    global CO2_ASKED
    global H_ASKED
    global L_ASKED
    if("idDivision" in message):
        div = message.split(',')[0].split(':')[1].lower()
        sensor = message.split(',')[1].split(':')[0]
        value = message.split(":")[2]
        if("kitchen" == div or "cozinha" == div):
            if("temperatura" == sensor):
                T_ASKED[0] = (int) (value)
            elif("co2" in message):
                CO2_ASKED[0] = (int) (value)
            elif("humidade" == sensor):
                H_ASKED[0] = (int) (value)
            elif("luz" == sensor):
                L_ASKED[0] = value
        elif("living room" == div or "sala" == div):
            if("temperatura" == sensor):
                T_ASKED[1] = (int) (value)
            elif("co2" == sensor):
                CO2_ASKED[1] = (int) (value)
            elif("humidade" == sensor):
                H_ASKED[1] = (int) (value)
            elif("luz" == sensor):
                L_ASKED[1] = value
        elif("bedroom" == div or "quarto" == div):
            if("temperatura" == sensor):
                T_ASKED[2] = (int) (value)
            elif("co2" == sensor):
                CO2_ASKED[2] = (int) (value)
            elif("humidade" == sensor):
                H_ASKED[2] = (int) (value)
            elif("luz" == sensor):
                L_ASKED[2] = value
        elif("bathroom" == div or "wc" == div):
            if("temperatura" == sensor):
                T_ASKED[3] = (int) (value)
            elif("co2" == sensor):
                CO2_ASKED[3] = (int) (value)
            elif("humidade" == sensor):
                H_ASKED[3] = (int) (value)
            elif("luz" == sensor):
                L_ASKED[3] = value
    # casa toda    
    else:
        if("temperatura" in message):
            global T_MIN
            T_MIN = (int)(message.split(":")[1])
            global T_MAX
            T_MAX = (int)(message.split(":")[1])
            global T_INICIAL
            T_INICIAL = (int)(message.split(":")[1])
        if("co2" in message):
            global CO2_MIN
            CO2_MIN = (int)(message.split(":")[1])
            global CO2_MAX
            CO2_MAX = (int)(message.split(":")[1])
            global CO2_INICIAL
            CO2_INICIAL = (int)(message.split(":")[1])
        if("humidade" in message):
            global H_MIN
            H_MIN = (int)(message.split(":")[1])
            global H_MAX
            H_MAX = (int)(message.split(":")[1])
            global H_INICIAL
            H_INICIAL = (int)(message.split(":")[1])
        global L_INICIAL
        global L_OFF
        global L_ON
        if("luzes" in message and "true" in message):
            L_INICIAL = True
            L_OFF = True
            L_ON = True
        if("luzes" in message and "false" in message):
            L_INICIAL = False
            L_OFF = False
            L_ON = False
        T_ASKED = [None, None, None, None]
        CO2_ASKED = [None, None, None, None]
        H_ASKED = [None, None, None, None]
        L_ASKED = [None, None, None, None]
            
    received_Kafka = ""
if __name__ == "__main__":
    main(sys.argv)