#!/bin/bash

set -e
exec python3 sensorsSimulator.py &
exec python3 costSimulator.py