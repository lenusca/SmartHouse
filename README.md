<h1> SmartHouse </h1>
<h2> About Us </h2>
SmartHouse is a project whose goal is to allow the management and monitoring of homes.  Making life easier for the homeowner, who will be able to control the environment of their home from a distance, saving time and expenses.
<h2> Features </h2>
<h3>Dashboard</h3>
Simple web-based page where the user can manage all the available features.
<h3> Sensors </h3>
<h4> Temperature </h4>
Temperature can be monitored and altered, for each room and the entire house, based on the user desire.
Given in ºC.
<h4> Humidity </h4>
Humidity sensors through out the house allow the customer to assess the information relying on graphics. 
Given in %.
<h4> Movement </h4>
With the help of motion sensors spread throughout strategic points, the client is able to get notifications (mail) when movement is detected on the property. Movement reports can also be found on the Dashboard.
Given in YES (if there is movement) or NO.
<h4> Lighting </h4>
The whole property lighting can be controlled and monitored via Dashboard, granting the user the means to save the utmost money.
Given in ON or OFF.
<h4> CO2 </h4>
CO2 can be monitored and altered, for each room and the entire house, based on the user desire.
Given in ppm.
<h2> Manual </h2>
To access web app, you need to have UA VPN, and url is : http://192.168.160.103:32060/
<p></p>
<p></p>
<p>Then if you want to check the temperature of your house in certain areas (or others features, it's done the same way) and placing the temperature higher so it can be warmer when you get home you need to:</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/1.png" width="100%"/>
      </td>
      <td>
        <ol>
          <li>Choose the "DIVISIONS" button on the side bar to be able to see wich divisions you are able to check</li>
          <li>Choose the division you want to check</li>
          <li>But if you want to check the average temperature ( or the average of other features) in the whole house you need to choose the "HOUSE" button on the side bar </li>
        </ol>
      </td>
  </table>
</div>
<p>
  And then you can:
</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/2.png" width="100%"/>
      </td>
      <td>
        <ul>
          <li>Check the actual temperature in your division</li>
          <li>And you can see the temperature of the same division in other days to compare just choose the day in the calendar and check it</li> 
  </table>
</div>
<p>
  The graphs:
</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/3.png" width="100%"/>
      </td>
      <td>
        <img src="images/4.png" width="100%"/>
      </td>
    </tr>
    <tr>
      <td>
        <img src="images/5.png" width="100%"/>
      </td>
      <td>
        <img src="images/6.png" width="100%"/>
      </td>
    </tr>
    <tr>
      <td>
        <img src="images/7.png" width="100%"/>
      </td>
    </tr>  
  </table>
</div>
<p>
  And now you want to change the temperature to be warmer when you get home so:
</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/8.png" width="100%"/>
      </td>
      <td>
        <ol>
          <li>Choose the "EDIT SENSORS" button on the side bar and there you can choose the whole house or just the divisions you want to change</li>
          <li>And after choosing you need to press in the icons the temperature icon and change the temperature</li>
        </ol>
      </td>
  </table>
</div>
<p>And if you want to change other sensors:</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/9.png" width="100%"/>
      </td>
      <td>
        <img src="images/10.png" width="100%"/>
      </td>
    </tr>
    <tr>
      <td>
        <img src="images/11.png" width="100%"/>
      </td> 
    <tr>
  </table>
</div>
<p>Then if you want to keep your eletrecity bill low you can check how and where your house is spending in certain areas (or in the whole house) and see what is speending the most you need to:</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/1.png" width="100%"/>
      </td>
      <td>
        <ol>
          <li>Choose the "DIVISIONS" button on the side bar to be able to see wich divisions you are able to check</li>
          <li>Choose the division you want to check</li>
          <li>But if you want to see the whole house you choose the "HOUSE" button on the side bar</li>
        </ol>
      </td>
  </table>
</div>
<div>
  <table>
    <tr>
      <td>
        <img src="images/12.png" width="100%"/>
      </td>
      <td>
        <ol>
          <li>Change that button to the costs option</li>
          <li>You can see the home appliances that are connected and how much they spent, but if you want to see how they have spent during the last days you need to choose a date</li>
        </ol>
      </td>
  </table>
</div>
<p>If you want to see the average costs in the whole house you need to:</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/15.png" width="100%"/>
      </td>
      <td>
        To see the average costs you need to pass the mouse over the "HOUSE" in the side bar and then choose the "COSTS"
      </td>
  </table>
</div>
<p>And then you can see the costs in one division or if you want to see a general picture of costs in the whole house you can see:</p>
<div>
  <table>
    <tr>
      <td>
        <img src="images/13.png" width="100%"/>
      </td>
      <td>
        <img src="images/14.png" width="100%"/>
      </td>
  </table>
</div>
<h2> For more info </h2>
<p></p>
https://es-2019-2020-p32.gitlab.io/smarthouse/
