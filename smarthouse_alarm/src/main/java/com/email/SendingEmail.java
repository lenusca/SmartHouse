package com.email;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.dbcontroller.UserRepository;
import com.example.smarthouse_alarm.SmarthouseAlarmApplication;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.HouseDivision;
import com.models.User;

@Component
@EnableScheduling
public class SendingEmail {
	@Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
	UserRepository userRepository;
	
	//scheduled 5min em 5 min
	@Scheduled(fixedRate= 300000*6)	
	public void sending() {
		// para ir buscar os utilizadores
		Iterable<User> listuser = userRepository.findAll();
		List<User> users = new ArrayList<>();
		listuser.forEach(e -> users.add(e));
		
		HouseDivision divisions = null;
		
		for(User u: listuser) {
			System.out.println(u.email);
		}
		
		if(!SmarthouseAlarmApplication.brokerData.equalsIgnoreCase("")) {
			
			ObjectMapper mapper = new ObjectMapper();
		
			InputStream inputStream = new ByteArrayInputStream(SmarthouseAlarmApplication.brokerData.getBytes());
			
			TypeReference<HouseDivision> typeReference = new TypeReference<HouseDivision>() {};
			try {
				divisions = mapper.readValue(inputStream, typeReference);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(divisions != null) {
				System.out.println("LENDO o movimento " + divisions.movimento );
				if(divisions.movimento == true) {
					System.out.println("SENDING");
					SimpleMailMessage msg = new SimpleMailMessage();
					for(User u: users) {
						System.out.println(u.email);
						msg.setTo(u.email);
	
				        msg.setSubject("SMARTHOUSE WARNING!!");
				        msg.setText("Is someone in the division "+divisions.idDivisao);
	
				        javaMailSender.send(msg);
					}
				}
			}
			
			
		}
	}

}
