package com.kafkacontroller;

import java.util.concurrent.CountDownLatch;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.example.smarthouse_alarm.SmarthouseAlarmApplication;

@Component
public class MessageListener {
	
	private CountDownLatch latch = new CountDownLatch(3);
    
    @KafkaListener(topics = "${jsa.kafka.topic}")
    public void listenGroupFoo(String message) {
    	
    	SmarthouseAlarmApplication.brokerData = message;
    	
    	System.out.println("Received: " + message);

        latch.countDown();
    }
}
