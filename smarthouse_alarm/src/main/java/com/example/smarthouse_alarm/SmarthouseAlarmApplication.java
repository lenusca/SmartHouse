package com.example.smarthouse_alarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.email.SendingEmail;
import com.kafkacontroller.MessageListener;


@SpringBootApplication
@ComponentScan({"com.dbcontroller", "com.email", "com.kafkacontroller"})
@EnableJpaRepositories({"com.dbcontroller"})
@EntityScan({"com.models"})
public class SmarthouseAlarmApplication {
	
	public static String brokerData="";
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(SmarthouseAlarmApplication.class, args);
		MessageListener listener = context.getBean(MessageListener.class);
		SendingEmail sending = new SendingEmail();
	}
  

}
