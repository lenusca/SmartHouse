package com.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.dbcontroller.CO2Repository;
import com.dbcontroller.DivisionCostRepository;
import com.dbcontroller.DivisionRepository;
import com.dbcontroller.HumidityRepository;
import com.dbcontroller.LightsRepository;
import com.dbcontroller.MainController;
import com.dbcontroller.MovementRepository;
import com.dbcontroller.TemperatureRepository;
import com.dbcontroller.UserRepository;
import com.models.CO2Sensor;
import com.models.CostsGraphs;
import com.models.Device;
import com.models.DivisionCost;
import com.models.DivisionData;
import com.models.DivisionGraphs;
import com.models.HouseCost;
import com.models.HouseCostGraph;
import com.models.HumiditySensor;
import com.models.LightsSensor;
import com.models.MovementSensor;
import com.models.TemperatureSensor;
import com.models.User;
import com.example.smarthouse.SmarthouseApplication;

@Component
@RestController
public class RestApi {
	static RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	CO2Repository co2Repository;
	
	@Autowired
	DivisionRepository divisionRepository;
	
	@Autowired
	HumidityRepository humidityRepository;
	
	@Autowired
	LightsRepository lightsRepository;
	
	@Autowired
	MovementRepository movementRepository;
	
	@Autowired
	TemperatureRepository temperatureRepository;
	
	@Autowired
	DivisionCostRepository divisioncostRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	MainController mainController;
	///////////////////////////////////////////////////////////////////SENSORS/////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////TODA A CASA///////////////////////////////////////////////////////
	/*VALORES ATUAIS*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/HouseInfo/", method = RequestMethod.GET)
	public ResponseEntity<com.models.DivisionData> HouseInfo(){
		mainController.addDivisions();
		DivisionData house = null;
		
		int co2WC = 0, co2R = 0, co2K = 0, co2S = 0, co2media = 0;
		int humidityWC = 0, humidityR= 0, humidityS = 0, humidityK = 0, humiditymedia = 0;
		int temperatureWC = 0, temperatureR = 0, temperatureS = 0, temperatureK = 0, temperaturemedia = 0;
		String time = "";
		boolean lightWC = false, lightR = false, lightS = false, lightK = false, lightmedia = false;
		boolean movementWC = false, movementR = false, movementS = false, movementK = false, movementmedia = false;
		
		Iterable<DivisionData> divisions = divisionRepository.findAll();
		List<DivisionData> listdivisions = new ArrayList<DivisionData>();
		divisions.forEach(e -> listdivisions.add(e));
		
		for(int i=0; i<listdivisions.size(); i++) {
			time = listdivisions.get(i).time;
			if(listdivisions.get(i).idDivision.equals("wc")) {
				co2WC = listdivisions.get(i).co2;
				humidityWC = listdivisions.get(i).humidity;
				temperatureWC = listdivisions.get(i).temperature;
				lightWC = listdivisions.get(i).light;
				movementWC = listdivisions.get(i).movement;
			}
			
			else if(listdivisions.get(i).idDivision.equals("quarto")) {
				co2R = listdivisions.get(i).co2;
				humidityR = listdivisions.get(i).humidity;
				temperatureR = listdivisions.get(i).temperature;
				lightR = listdivisions.get(i).light;
				movementR = listdivisions.get(i).movement;
			}
			
			else if(listdivisions.get(i).idDivision.equals("sala")) {
				co2S = listdivisions.get(i).co2;
				humidityS = listdivisions.get(i).humidity;
				temperatureS = listdivisions.get(i).temperature;
				lightS = listdivisions.get(i).light;
				movementS = listdivisions.get(i).movement;
			}
			
			else if(listdivisions.get(i).idDivision.equals("cozinha")) {
				co2K = listdivisions.get(i).co2;
				humidityK = listdivisions.get(i).humidity;
				temperatureK = listdivisions.get(i).temperature;
				lightK = listdivisions.get(i).light;
				movementK = listdivisions.get(i).movement;
			}
		}
		
		int countBoolean = 0;
		co2media = (co2WC + co2R + co2K + co2S)/4;
		humiditymedia = (humidityWC + humidityR + humidityS + humidityK)/4;
		temperaturemedia = (temperatureWC + temperatureR + temperatureS + temperatureK)/4; 
		if(lightWC == true) {
			countBoolean += 1;
		}
		
		if(lightR == true) {
			countBoolean += 1;
		}
		
		if(lightS == true) {
			countBoolean += 1;
		}
		
		if(lightK == true) {
			countBoolean += 1;
		}
		
		if(countBoolean >= 2) {
			lightmedia = true;
		}
		
		countBoolean = 0;
		
		if(movementWC == true) {
			countBoolean += 1;
		}
		
		if(movementR == true) {
			countBoolean += 1;
		}
		
		if(movementS == true) {
			countBoolean += 1;
		}
		
		if(movementK == true) {
			countBoolean += 1;
		}
		
		if(countBoolean >= 2) {
			movementmedia = true;
		}
		
		house = new DivisionData(time, "House", co2media, humiditymedia, lightmedia, movementmedia, temperaturemedia);
		
		return new ResponseEntity<com.models.DivisionData>(house, HttpStatus.OK);
	}
	
	/*VALORES PARA GRAFICO*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/HouseGraphs/", method = RequestMethod.GET)
	public ResponseEntity<com.models.DivisionGraphs> HouseGraphs(@RequestParam("time") String time){
		DivisionGraphs houseData = new DivisionGraphs();
		
		int co2WC = 0, co2R = 0, co2K = 0, co2S = 0, co2media = 0;
		int humidityWC = 0, humidityR= 0, humidityS = 0, humidityK = 0, humiditymedia = 0;
		int temperatureWC = 0, temperatureR = 0, temperatureS = 0, temperatureK = 0, temperaturemedia = 0;
		boolean lightWC = false, lightR = false, lightS = false, lightK = false, lightmedia = false;
		boolean movementWC = false, movementR = false, movementS = false, movementK = false, movementmedia = false;
		
		Iterable<DivisionData> divisions = divisionRepository.findAll();
		List<DivisionData> listdivisions = new ArrayList<DivisionData>();
		divisions.forEach(e -> listdivisions.add(e));

		for(int i=0; i<listdivisions.size(); i++) {
			if(listdivisions.get(i).time.split("-")[0].equals(time.split("/")[0])  && listdivisions.get(i).time.split("-")[1].equals(time.split("/")[1]) && listdivisions.get(i).time.split("-")[2].split(" ")[0].equals(time.split("/")[2])) {
				if(listdivisions.get(i).idDivision.equals("wc")) {
					co2WC = listdivisions.get(i).co2;
					humidityWC = listdivisions.get(i).humidity;
					temperatureWC = listdivisions.get(i).temperature;
					lightWC = listdivisions.get(i).light;
					movementWC = listdivisions.get(i).movement;
					int countBoolean = 0;
					co2media = (co2WC + co2R + co2K + co2S)/4;
					humiditymedia = (humidityWC + humidityR + humidityS + humidityK)/4;
					temperaturemedia = (temperatureWC + temperatureR + temperatureS + temperatureK)/4; 
					if(lightWC == true) {
						countBoolean += 1;
					}
					
					if(lightR == true) {
						countBoolean += 1;
					}
					
					if(lightS == true) {
						countBoolean += 1;
					}
					
					if(lightK == true) {
						countBoolean += 1;
					}
					
					if(countBoolean >= 2) {
						lightmedia = true;
					}
					
					countBoolean = 0;
					
					if(movementWC == true) {
						countBoolean += 1;
					}
					
					if(movementR == true) {
						countBoolean += 1;
					}
					
					if(movementS == true) {
						countBoolean += 1;
					}
					
					if(movementK == true) {
						countBoolean += 1;
					}
					
					if(countBoolean >= 2) {
						movementmedia = true;
					}
					
					CO2Sensor co2 = new CO2Sensor();
					co2.time = listdivisions.get(i).time;
					co2.value = co2media;				
			
					houseData.co2.add(co2);

					
					HumiditySensor humidity = new HumiditySensor();
					humidity.time = listdivisions.get(i).time;
					humidity.value = humiditymedia;				
					houseData.humidity.add(humidity);
					
					LightsSensor light = new LightsSensor();
					light.time = listdivisions.get(i).time;
					light.value = lightmedia;				
					houseData.light.add(light);
					
					MovementSensor movement = new MovementSensor();
					movement.time = listdivisions.get(i).time;
					movement.value = movementmedia;				
					houseData.movement.add(movement);
					
					TemperatureSensor temperature = new TemperatureSensor();
					temperature.time = listdivisions.get(i).time;
					temperature.value = temperaturemedia;				
					houseData.temperature.add(temperature);
				}
				
				else if(listdivisions.get(i).idDivision.equals("quarto")) {
					co2R = listdivisions.get(i).co2;
					humidityR = listdivisions.get(i).humidity;
					temperatureR = listdivisions.get(i).temperature;
					lightR = listdivisions.get(i).light;
					movementR = listdivisions.get(i).movement;
				}
				
				else if(listdivisions.get(i).idDivision.equals("sala")) {
					co2S = listdivisions.get(i).co2;
					humidityS = listdivisions.get(i).humidity;
					temperatureS = listdivisions.get(i).temperature;
					lightS = listdivisions.get(i).light;
					movementS = listdivisions.get(i).movement;
				}
				
				else if(listdivisions.get(i).idDivision.equals("cozinha")) {
					co2K = listdivisions.get(i).co2;
					humidityK = listdivisions.get(i).humidity;
					temperatureK = listdivisions.get(i).temperature;
					lightK = listdivisions.get(i).light;
					movementK = listdivisions.get(i).movement;
				}
				
			}	
		}
		
		return new ResponseEntity<com.models.DivisionGraphs>(houseData, HttpStatus.OK);
	}
	
	/*ALTERAÇÃO DOS VALORES*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/EditSensorsHouse/", method = RequestMethod.GET)
	public void editSensorsHouse(@RequestParam("idSensor") String idSensor,  @RequestParam("value") String value){
		//KAFKA PARA ENVIAR OS DADOS
		System.out.println("#######################################################################################################");
		System.out.println(idSensor + ":"+ value);
		SmarthouseApplication.producer.sendMessageToFiltered(idSensor + ":" + value);
	}
	
	////////////////////////////////////////////////////INFO DE UMA DIVISÃO//////////////////////////////////////////////
	/*VALORES ATUAIS*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/DivisionInfo/", method = RequestMethod.GET)
	public ResponseEntity<com.models.DivisionData> DivisionInfo(@RequestParam("idDivision") String idDivision){
		mainController.addDivisions();
		DivisionData division = null;
		Iterable<DivisionData> divisions = divisionRepository.findAll();
		List<DivisionData> listdivisions = new ArrayList<DivisionData>();
		divisions.forEach(e -> listdivisions.add(e));
		for(int i=0; i<listdivisions.size(); i++) {		
			if(listdivisions.get(i).idDivision.equals(idDivision)) {
				if(idDivision.equals("wc")) {
					division = new DivisionData(listdivisions.get(i).time, "Bathroom", listdivisions.get(i).co2, listdivisions.get(i).humidity, listdivisions.get(i).light, listdivisions.get(i).movement, listdivisions.get(i).temperature); 
				}
				
				else if(idDivision.equals("quarto")) {
					division = new DivisionData(listdivisions.get(i).time, "Bedroom", listdivisions.get(i).co2, listdivisions.get(i).humidity, listdivisions.get(i).light, listdivisions.get(i).movement, listdivisions.get(i).temperature); 
				}
				
				else if(idDivision.equals("sala")) {
					division = new DivisionData(listdivisions.get(i).time, "Living Room", listdivisions.get(i).co2, listdivisions.get(i).humidity, listdivisions.get(i).light, listdivisions.get(i).movement, listdivisions.get(i).temperature); 
				}
				
				else if(idDivision.equals("cozinha")) {
					division = new DivisionData(listdivisions.get(i).time, "Kitchen", listdivisions.get(i).co2, listdivisions.get(i).humidity, listdivisions.get(i).light, listdivisions.get(i).movement, listdivisions.get(i).temperature); 
				}
			}
		}

		
		return new ResponseEntity<com.models.DivisionData>(division, HttpStatus.OK);
	}
	
	/*VALORES PARA GRAFICO*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/DivisionGraph/", method = RequestMethod.GET)
	public ResponseEntity<com.models.DivisionGraphs> DivisionGraph(@RequestParam("idDivision") String idDivision,  @RequestParam("time") String time){
		DivisionGraphs divisionData = new DivisionGraphs();
		Iterable<DivisionData> divisions = divisionRepository.findAll();
		List<DivisionData> listdivisions = new ArrayList<DivisionData>();
		divisions.forEach(e -> listdivisions.add(e));

		for(int i=0; i<listdivisions.size(); i++) {
			if(listdivisions.get(i).idDivision.equals(idDivision) && listdivisions.get(i).time.split("-")[0].equals(time.split("/")[0])  && listdivisions.get(i).time.split("-")[1].equals(time.split("/")[1]) && listdivisions.get(i).time.split("-")[2].split(" ")[0].equals(time.split("/")[2])) {
				CO2Sensor co2 = new CO2Sensor();
				co2.time = listdivisions.get(i).time;
				co2.value = listdivisions.get(i).co2;				
		
				divisionData.co2.add(co2);
	
				
				HumiditySensor humidity = new HumiditySensor();
				humidity.time = listdivisions.get(i).time;
				humidity.value = listdivisions.get(i).humidity;				
				divisionData.humidity.add(humidity);
				
				LightsSensor light = new LightsSensor();
				light.time = listdivisions.get(i).time;
				light.value = listdivisions.get(i).light;				
				divisionData.light.add(light);
				
				MovementSensor movement = new MovementSensor();
				movement.time = listdivisions.get(i).time;
				movement.value = listdivisions.get(i).movement;				
				divisionData.movement.add(movement);
				
				TemperatureSensor temperature = new TemperatureSensor();
				temperature.time = listdivisions.get(i).time;
				temperature.value = listdivisions.get(i).temperature;				
				divisionData.temperature.add(temperature);
			}
		}
		return new ResponseEntity<com.models.DivisionGraphs>(divisionData, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/EditSensorsDivision/", method = RequestMethod.GET)
	public void editSensorsDivision(@RequestParam("idDivision") String idDivision, @RequestParam("idSensor") String idSensor,  @RequestParam("value") String value){
		//KAFKA PARA ENVIAR OS DADOS
		System.out.println("#######################################################################################################");
		System.out.println("idDivision :"+idDivision+","+idSensor + ":"+ value);
		SmarthouseApplication.producer.sendMessageToFiltered("idDivision :"+idDivision+","+idSensor + ":"+ value);
	}

/////////////////////////////////////////////////////////////////////GASTOS/////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////TODA A CASA///////////////////////////////////////////////////
	/*VALORES ATUAIS*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/CostHouse/", method = RequestMethod.GET)
	public ResponseEntity<com.models.HouseCost> CostHouse(){
		mainController.addCosts();
		HouseCost house = null;
		Iterable<DivisionCost> divisions = divisioncostRepository.findAll();
		List<DivisionCost> listdivisions = new ArrayList<DivisionCost>();
		divisions.forEach(e -> listdivisions.add(e));
		double costsWC = 0, costsR = 0, costsS = 0, costsK = 0;
		
		for(int i=0; i<listdivisions.size(); i++) {		
			// soma de todos os custos de cada divisão
			if(listdivisions.get(i).idDivision.equals("wc")) {
				costsWC = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
			}
			
			else if(listdivisions.get(i).idDivision.equals("quarto")) {
				costsR = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
			}
			
			else if(listdivisions.get(i).idDivision.equals("sala")) {
				costsS = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
			}
			
			else if(listdivisions.get(i).idDivision.equals("cozinha")) {
				costsK = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
			}
			house = new HouseCost(listdivisions.get(i).time, "House", "Bathroom", costsWC, "Bedroom", costsR, "Living Room", costsS, "Kitchen", costsK);
		}
		
		return new ResponseEntity<com.models.HouseCost>(house, HttpStatus.OK);
		}
	/*Valores para gráfico*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/GraphHouseCost/", method = RequestMethod.GET)
	public ResponseEntity<com.models.HouseCostGraph> CostHouse(@RequestParam("time") String time){
		HouseCostGraph house = new HouseCostGraph();
		
		Iterable<DivisionCost> divisions = divisioncostRepository.findAll();
		List<DivisionCost> listdivisions = new ArrayList<DivisionCost>();
		divisions.forEach(e -> listdivisions.add(e));
		double costsWC = 0, costsR = 0, costsS = 0, costsK = 0, costsTotal = 0;
		
		for(int i=0; i<listdivisions.size(); i++) {		
			// soma de todos os custos de cada divisão
			if(listdivisions.get(i).time.split("-")[0].equals(time.split("/")[0])  && listdivisions.get(i).time.split("-")[1].equals(time.split("/")[1]) && listdivisions.get(i).time.split("-")[2].split(" ")[0].equals(time.split("/")[2])) {
				if(listdivisions.get(i).idDivision.equals("wc")) {
					costsWC = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
					costsTotal = costsWC + costsR + costsS + costsK;
					
					Device quarto = new Device();
					quarto.time = listdivisions.get(i).time;
					quarto.name = "Bedroom";
					quarto.value = costsR;
					house.division1.add(quarto);
					
					Device sala = new Device();
					sala.time = listdivisions.get(i).time;
					sala.name = "Living Room";
					sala.value = costsS;
					house.division2.add(sala);
					
					Device cozinha = new Device();
					cozinha.time = listdivisions.get(i).time;
					cozinha.name = "Kitchen";
					cozinha.value = costsK;
					house.division3.add(cozinha);
					
					Device wc = new Device();
					wc.time = listdivisions.get(i).time;
					wc.name = "Bathroom";
					wc.value = costsWC;
					house.division4.add(wc);
					
					Device total = new Device();
					total.time = listdivisions.get(i).time;
					total.name = "House";
					total.value = costsTotal;
					house.total.add(total);
				}
				
				else if(listdivisions.get(i).idDivision.equals("quarto")) {
					costsR = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
				}
				
				else if(listdivisions.get(i).idDivision.equals("sala")) {
					costsS = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
				}
				
				else if(listdivisions.get(i).idDivision.equals("cozinha")) {
					costsK = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
				}
			}
		}
		
		return new ResponseEntity<com.models.HouseCostGraph>(house, HttpStatus.OK);
	}
	////////////////////////////////////////////////////INFO DE UMA DIVISÃO//////////////////////////////////////////////
	/*VALORES ATUAIS*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/CostDivision/", method = RequestMethod.GET)
	public ResponseEntity<com.models.DivisionCost> CostDivision(@RequestParam("idDivision") String idDivision){
		mainController.addCosts();
		DivisionCost division = null;
		Iterable<DivisionCost> divisions = divisioncostRepository.findAll();
		List<DivisionCost> listdivisions = new ArrayList<DivisionCost>();
		divisions.forEach(e -> listdivisions.add(e));
		
		for(int i=0; i<listdivisions.size(); i++) {		
			if(listdivisions.get(i).idDivision.equals(idDivision)) {
				if(idDivision.equals("wc")) {
					division = new DivisionCost(listdivisions.get(i).time, "Bathroom", listdivisions.get(i).d1Name, listdivisions.get(i).d1, listdivisions.get(i).d2Name, listdivisions.get(i).d2, listdivisions.get(i).d3Name, listdivisions.get(i).d3);
				}
				
				else if(idDivision.equals("quarto")) {
					division = new DivisionCost(listdivisions.get(i).time, "Bedroom", listdivisions.get(i).d1Name, listdivisions.get(i).d1, listdivisions.get(i).d2Name, listdivisions.get(i).d2, listdivisions.get(i).d3Name, listdivisions.get(i).d3); 
				}
				
				else if(idDivision.equals("sala")) {
					division = new DivisionCost(listdivisions.get(i).time, "Living Room", listdivisions.get(i).d1Name, listdivisions.get(i).d1, listdivisions.get(i).d2Name, listdivisions.get(i).d2, listdivisions.get(i).d3Name, listdivisions.get(i).d3);
			    }
				
				else if(idDivision.equals("cozinha")) {
					division = new DivisionCost(listdivisions.get(i).time, "Kitchen", listdivisions.get(i).d1Name, listdivisions.get(i).d1, listdivisions.get(i).d2Name, listdivisions.get(i).d2, listdivisions.get(i).d3Name, listdivisions.get(i).d3);
				}
			}
		}
		
		return new ResponseEntity<com.models.DivisionCost>(division, HttpStatus.OK);
	}

	/*VALORES PARA GRAFICO*/
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/DivisionCostsGraph/", method = RequestMethod.GET)
	public ResponseEntity<com.models.CostsGraphs> CostGraph(@RequestParam("idDivision") String idDivision,  @RequestParam("time") String time){
		CostsGraphs divisionCosts = new CostsGraphs();
		Iterable<DivisionCost> divisions = divisioncostRepository.findAll();
		List<DivisionCost> listdivisions = new ArrayList<DivisionCost>();
		divisions.forEach(e -> listdivisions.add(e));
	
		for(int i=0; i<listdivisions.size(); i++) {
			if(listdivisions.get(i).idDivision.equals(idDivision) && listdivisions.get(i).time.split("-")[0].equals(time.split("/")[0])  && listdivisions.get(i).time.split("-")[1].equals(time.split("/")[1]) && listdivisions.get(i).time.split("-")[2].split(" ")[0].equals(time.split("/")[2])) {
				Device d1 = new Device();
				d1.time = listdivisions.get(i).time;
				d1.value = listdivisions.get(i).d1;				
				divisionCosts.d1.add(d1);
	
				Device d2 = new Device();
				d2.time = listdivisions.get(i).time;
				d2.value = listdivisions.get(i).d2;				
				divisionCosts.d2.add(d2);
				
				Device d3 = new Device();
				d3.time = listdivisions.get(i).time;
				d3.value = listdivisions.get(i).d3;				
				divisionCosts.d3.add(d3);
				
				Device media = new Device();
				media.time = listdivisions.get(i).time;
				media.value = listdivisions.get(i).d1 + listdivisions.get(i).d2 + listdivisions.get(i).d3;
				divisionCosts.media.add(media);
			}
		}
		return new ResponseEntity<com.models.CostsGraphs>(divisionCosts, HttpStatus.OK);
	}
	
/////////////////////////////////////////////////////////////////////////////UTILIZADOR/////////////////////////////////////////////////////////////////
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/CreateAccount/", method = RequestMethod.GET)
	public void createAccount(@RequestParam("email") String email,  @RequestParam("password") String password){
		System.out.println(email);
		User user = new User(email, password);
		userRepository.save(user);
	}
	
	@CrossOrigin(origins = "http://192.168.160.103:32060")
	@RequestMapping(value="/Login/", method = RequestMethod.GET)
	public ResponseEntity<List<com.models.User>>  Login(){
		Iterable<User> listuser = userRepository.findAll();
		List<User> users = new ArrayList<>();
		listuser.forEach(e -> users.add(e));
		return new ResponseEntity<List<com.models.User>>(users, HttpStatus.OK);
	}
}


