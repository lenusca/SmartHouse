package com.example.smarthouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.api.RestApi;
import com.dbcontroller.MainController;
import com.kafkacontroller.MessageListener;
import com.kafkacontroller.MessageProducer;
import com.models.HouseDivision;
import com.restclient.CallRestService;

@SpringBootApplication
@ComponentScan({"com.dbcontroller", "com.api"})
@EnableJpaRepositories({"com.dbcontroller"})
@EntityScan({"com.models"})
public class SmarthouseApplication {
	
	//private static final Logger log = LoggerFactory.getLogger(SmarthouseApplication.class);
	
	public static String brokerData="";

	/**
	 * @variable brokerCostData cost that comes from kafka broker
	 */
	public static String brokerCostData="";
	
	public static MessageProducer producer;
	
	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		ConfigurableApplicationContext context = SpringApplication.run(SmarthouseApplication.class, args);
		MessageListener listener = context.getBean(MessageListener.class);
		producer = context.getBean(MessageProducer.class);
		MainController maincontroller = context.getBean(MainController.class);
		CallRestService restService = new CallRestService();
		RestApi restapi = context.getBean(RestApi.class);
	}
	
	@Bean
    public MessageListener messageListener() {
        return new MessageListener();
    }
	
	@Bean
    public MessageProducer messageProducer() {
        return new MessageProducer();
    }
  
}
