package com.restclient;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.dbcontroller.UserRepository;
import com.example.smarthouse.SmarthouseApplication;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.models.*;
@Component
public class CallRestService {
	static RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
	UserRepository userRepository;
	
	public List<HouseDivision> getDivisions() {
		List<HouseDivision> divisions = null;
		if(!SmarthouseApplication.brokerData.equalsIgnoreCase("")) {
			ObjectMapper mapper = new ObjectMapper();
		
			InputStream inputStream = new ByteArrayInputStream(SmarthouseApplication.brokerData.getBytes());
			
			TypeReference<List<HouseDivision>> typeReference = new TypeReference<List<HouseDivision>>() {};
			try {
				divisions = mapper.readValue(inputStream, typeReference);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(HouseDivision d : divisions) {
				System.out.print(d.idDivision);
			}
			
		}
		return divisions;
	}
	
	public List<Cost> getCosts() {
		List<Cost> costs = null;
		if(!SmarthouseApplication.brokerCostData.equalsIgnoreCase("")) {
			
			ObjectMapper mapper = new ObjectMapper();
			
			
			InputStream inputStream = new ByteArrayInputStream(SmarthouseApplication.brokerCostData.getBytes());
			TypeReference<List<Cost>> typeReference = new TypeReference<List<Cost>>() {};
			
			try {
				
				costs = mapper.readValue(inputStream, typeReference);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return costs;
	}
	 
	// scheduled 2 em 2horas
	@Scheduled(fixedRate=3600000*2)
	public void sendEmail() {
		// para ir buscar os utilizadores
		Iterable<User> listuser = userRepository.findAll();
		List<User> users = new ArrayList<>();
		listuser.forEach(e -> users.add(e));
		
		List<HouseDivision> divisions = null;
		
		if(!SmarthouseApplication.brokerData.equalsIgnoreCase("")) {
			
			ObjectMapper mapper = new ObjectMapper();
		
			InputStream inputStream = new ByteArrayInputStream(SmarthouseApplication.brokerData.getBytes());
			
			TypeReference<List<HouseDivision>> typeReference = new TypeReference<List<HouseDivision>>() {};
			try {
				divisions = mapper.readValue(inputStream, typeReference);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(HouseDivision d : divisions) {
				if(d.movement.value == true) {
					SimpleMailMessage msg = new SimpleMailMessage();
					for(User u: users) {
						System.out.println(u.email);
						msg.setTo(u.email);

				        msg.setSubject("SMARTHOUSE WARNING!!");
				        msg.setText("Is someone in the division "+d.idDivision);

				        javaMailSender.send(msg);
					}
					
				}
			}
			
		}
		
		
		
	}
}
