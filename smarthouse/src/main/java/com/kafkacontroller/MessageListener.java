package com.kafkacontroller;

import java.util.concurrent.CountDownLatch;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

import com.example.smarthouse.SmarthouseApplication;



public class MessageListener {
	
	private CountDownLatch latch = new CountDownLatch(3);
	
    private CountDownLatch greetingLatch = new CountDownLatch(1);
    
    @KafkaListener(topics = "${jsa.kafka.topic}")
    public void listenGroupFoo(String message) {

    	SmarthouseApplication.brokerData = message;
    	
    	System.out.println("Received: " + message);

        latch.countDown();
    }
    
    @KafkaListener(topics = "${jsa.kafka.second}")
    public void listenGroupCost(String message) {

    	SmarthouseApplication.brokerCostData = message;
    	
    	System.out.println("Received: " + message);
    	
    	
    	greetingLatch.countDown();
    }
    
    


}
