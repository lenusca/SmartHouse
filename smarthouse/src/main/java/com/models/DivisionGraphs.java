package com.models;

import java.util.*;

public class DivisionGraphs {
	public List<CO2Sensor> co2 = new ArrayList<CO2Sensor>();
	public List<HumiditySensor> humidity = new ArrayList<HumiditySensor>();
	public List<LightsSensor> light = new ArrayList<LightsSensor>();
	public List<MovementSensor> movement = new ArrayList<MovementSensor>();
	public List<TemperatureSensor> temperature = new ArrayList<TemperatureSensor>();
	
	public DivisionGraphs() {
		
	}

	public List<CO2Sensor> getCo2() {
		return co2;
	}

	public void setCo2(List<CO2Sensor> co2) {
		this.co2 = co2;
	}

	public List<HumiditySensor> getHumidity() {
		return humidity;
	}

	public void setHumidity(List<HumiditySensor> humidity) {
		this.humidity = humidity;
	}

	public List<LightsSensor> getLight() {
		return light;
	}

	public void setLight(List<LightsSensor> light) {
		this.light = light;
	}

	public List<MovementSensor> getMovement() {
		return movement;
	}

	public void setMovement(List<MovementSensor> movement) {
		this.movement = movement;
	}

	public List<TemperatureSensor> getTemperature() {
		return temperature;
	}

	public void setTemperature(List<TemperatureSensor> temperature) {
		this.temperature = temperature;
	}

	@Override
	public String toString() {
		return "DivisionGraphs [co2=" + co2 + ", humidity=" + humidity + ", light=" + light + ", movement=" + movement
				+ ", temperature=" + temperature + "]";
	}

	
	
	
}
