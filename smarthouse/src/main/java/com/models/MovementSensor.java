package com.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovementSensor {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int id;
	public String time;
	public Boolean value;
	
	public MovementSensor() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "MovementSensor [id=" + id + ", time=" + time + ", value=" + value + "]";
	}
	
	
	
	
	
	
	
	
}
