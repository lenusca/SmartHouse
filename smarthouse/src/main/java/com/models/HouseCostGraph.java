package com.models;

import java.util.ArrayList;
import java.util.List;

public class HouseCostGraph {
	public List<Device> division1 = new ArrayList<Device>();
	public List<Device> division2 = new ArrayList<Device>();
	public List<Device> division3 = new ArrayList<Device>();
	public List<Device> division4 = new ArrayList<Device>();
	public List<Device> total = new ArrayList<Device>();
	
	public HouseCostGraph() {
		
	}

	public List<Device> getDivision1() {
		return division1;
	}

	public void setDivision1(List<Device> division1) {
		this.division1 = division1;
	}

	public List<Device> getDivision2() {
		return division2;
	}

	public void setDivision2(List<Device> division2) {
		this.division2 = division2;
	}

	public List<Device> getDivision3() {
		return division3;
	}

	public void setDivision3(List<Device> division3) {
		this.division3 = division3;
	}

	public List<Device> getTotal() {
		return total;
	}

	public void setTotal(List<Device> total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "HouseCostGraph [division1=" + division1 + ", division2=" + division2 + ", division3=" + division3
				+ ", total=" + total + "]";
	}
	
	
}
