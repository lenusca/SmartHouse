package com.models;

public class HouseDivision {
	
	public int id;
	public String time;
	public String idDivision;
	public CO2Sensor co2;
	public HumiditySensor humidity;
	public LightsSensor light;
	public MovementSensor movement;
	public TemperatureSensor temperature;
	
	public HouseDivision() {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getIdDivision() {
		return idDivision;
	}

	public void setIdDivision(String idDivision) {
		this.idDivision = idDivision;
	}

	public CO2Sensor getCo2() {
		return co2;
	}

	public void setCo2(CO2Sensor co2) {
		this.co2 = co2;
	}

	public HumiditySensor getHumidity() {
		return humidity;
	}

	public void setHumidity(HumiditySensor humidity) {
		this.humidity = humidity;
	}

	public LightsSensor getLight() {
		return light;
	}

	public void setLight(LightsSensor light) {
		this.light = light;
	}

	public MovementSensor getMovement() {
		return movement;
	}

	public void setMovement(MovementSensor movement) {
		this.movement = movement;
	}

	public TemperatureSensor getTemperature() {
		return temperature;
	}

	public void setTemperature(TemperatureSensor temperature) {
		this.temperature = temperature;
	}

	@Override
	public String toString() {
		return "HouseDivision [id=" + id + ", time=" + time + ", idDivision=" + idDivision + ", co2=" + co2
				+ ", humidity=" + humidity + ", light=" + light + ", movement=" + movement + ", temperature="
				+ temperature + "]";
	}
	
	
	
	
}
