package com.models;

public class HouseCost {
	public String time;
	public String id;
	public String idDivision1;
	public double d1;
	public String idDivision2;
	public double d2;
	public String idDivision3;
	public double d3;
	public String idDivision4;
	public double d4;
	
	public HouseCost() {
		
	}
	
	public HouseCost(String time, String id, String idDivision1, double d1, String idDivision2, double d2, String idDivision3, double d3, String idDivision4, double d4) {
		this.time = time; 
		this.id = id;
		this.idDivision1 = idDivision1;
		this.d1 = d1;
		this.idDivision2 = idDivision2;
		this.d2 = d2;
		this.idDivision3 = idDivision3;
		this.d3 = d3;
		this.idDivision4 = idDivision4;
		this.d4 = d4;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdDivision1() {
		return idDivision1;
	}

	public void setIdDivision1(String idDivision1) {
		this.idDivision1 = idDivision1;
	}

	public double getD1() {
		return d1;
	}

	public void setD1(double d1) {
		this.d1 = d1;
	}

	public String getIdDivision2() {
		return idDivision2;
	}

	public void setIdDivision2(String idDivision2) {
		this.idDivision2 = idDivision2;
	}

	public double getD2() {
		return d2;
	}

	public void setD2(double d2) {
		this.d2 = d2;
	}

	public String getIdDivision3() {
		return idDivision3;
	}

	public void setIdDivision3(String idDivision3) {
		this.idDivision3 = idDivision3;
	}

	public double getD3() {
		return d3;
	}

	public void setD3(double d3) {
		this.d3 = d3;
	}

	public String getIdDivision4() {
		return idDivision4;
	}

	public void setIdDivision4(String idDivision4) {
		this.idDivision4 = idDivision4;
	}

	public double getD4() {
		return d4;
	}

	public void setD4(double d4) {
		this.d4 = d4;
	}

	@Override
	public String toString() {
		return "HouseCost [time=" + time + ", id=" + id + ", idDivision1=" + idDivision1 + ", d1=" + d1
				+ ", idDivision2=" + idDivision2 + ", d2=" + d2 + ", idDivision3=" + idDivision3 + ", d3=" + d3
				+ ", idDivision4=" + idDivision4 + ", d4=" + d4 + "]";
	}

	
	
}
