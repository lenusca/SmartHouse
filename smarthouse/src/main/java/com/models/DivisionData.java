package com.models;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class DivisionData {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int id;
	public String time;
	public String idDivision;
	public int co2;
	public int humidity;
	public boolean light;
	public boolean movement;
	public int temperature;
	
	public DivisionData() {
		
	}
	
	public DivisionData(String time, String idDivision, int co2, int humidity, boolean light, boolean movement, int temperature) {
		this.idDivision = idDivision;
		this.time = time;
		this.co2 = co2;
		this.humidity = humidity;
		this.light = light;
		this.temperature = temperature;
		this.movement = movement;
	}
	
	public String getIdDivision() {
		return idDivision;
	}
	
	public void setIdDivision(String idDivision) {
		this.idDivision = idDivision;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getCo2() {
		return co2;
	}

	public void setCo2(int co2) {
		this.co2 = co2;
	}

	public int getHumidity() {
		return humidity;
	}

	public void setHumidity(int humidity) {
		this.humidity = humidity;
	}

	public boolean isLight() {
		return light;
	}

	public void setLight(boolean light) {
		this.light = light;
	}

	public boolean isMovement() {
		return movement;
	}

	public void setMovement(boolean movement) {
		this.movement = movement;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	@Override
	public String toString() {
		return "DivisionData [id=" + id + ", time=" + time + ", idDivision=" + idDivision + ", co2=" + co2
				+ ", humidity=" + humidity + ", light=" + light + ", movement=" + movement + ", temperature="
				+ temperature + "]";
	}

	
}
