package com.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class DivisionCost {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public int id;
	public String time;
	public String idDivision;
	public String d1Name;
	public double d1;
	public String d2Name;
	public double d2;
	public String d3Name;
	public double d3;
	
	public DivisionCost() {
		
	}
	
	public DivisionCost(String time, String idDivision, String d1Name, double d1, String d2Name, double d2, String d3Name, double d3) {
		this.time = time;
		this.idDivision = idDivision;
		this.d1Name = d1Name;
		this.d1 = d1;
		this.d2Name = d2Name;
		this.d2 = d2;
		this.d3Name = d3Name;
		this.d3 = d3;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getIdDivision() {
		return idDivision;
	}
	public void setIdDivision(String idDivision) {
		this.idDivision = idDivision;
	}
	public double getD1() {
		return d1;
	}
	public void setD1(double d1) {
		this.d1 = d1;
	}
	public double getD2() {
		return d2;
	}
	public void setD2(double d2) {
		this.d2 = d2;
	}
	public double getD3() {
		return d3;
	}
	public void setD3(double d3) {
		this.d3 = d3;
	}
	
	public String getD1Name() {
		return d1Name;
	}

	public void setD1Name(String d1Name) {
		this.d1Name = d1Name;
	}

	public String getD2Name() {
		return d2Name;
	}

	public void setD2Name(String d2Name) {
		this.d2Name = d2Name;
	}

	public String getD3Name() {
		return d3Name;
	}

	public void setD3Name(String d3Name) {
		this.d3Name = d3Name;
	}

	@Override
	public String toString() {
		return "divisionCost [id=" + id + ", time=" + time + ", idDivision=" + idDivision + ", d1=" + d1 + ", d2=" + d2
				+ ", d3=" + d3 + "]";
	}
	
	
}
