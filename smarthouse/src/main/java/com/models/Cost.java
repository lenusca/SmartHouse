package com.models;


public class Cost {
	
	public String idDivisao;
	public Device d1;
	public Device d2;
	public Device d3;
	
	public String getIdDivision() {
		return idDivisao;
	}
	
	public void setIdDivision(String idDivision) {
		this.idDivisao = idDivision;
	}
	
	public Device getD1() {
		return d1;
	}
	
	public void setD1(Device d1) {
		this.d1 = d1;
	}
	
	public Device getD2() {
		return d2;
	}
	
	public void setD2(Device d2) {
		this.d2 = d2;
	}
	
	public Device getD3() {
		return d3;
	}
	
	public void setD3(Device d3) {
		this.d3 = d3;
	}

	@Override
	public String toString() {
		return "Cost [ idDivision=" + idDivisao + ", d1=" + d1 + ", d2=" + d2 + ", d3=" + d3 + "]";
	}
	
	
	
}
