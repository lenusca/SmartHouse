package com.models;

import java.util.ArrayList;
import java.util.List;

public class CostsGraphs {
	public List<Device> d1 = new ArrayList<Device>();
	public List<Device> d2 = new ArrayList<Device>();
	public List<Device> d3 = new ArrayList<Device>();
	public List<Device> media = new ArrayList<Device>();
	
	public CostsGraphs() {
		
	}

	public List<Device> getD1() {
		return d1;
	}

	public void setD1(List<Device> d1) {
		this.d1 = d1;
	}

	public List<Device> getD2() {
		return d2;
	}

	public void setD2(List<Device> d2) {
		this.d2 = d2;
	}

	public List<Device> getD3() {
		return d3;
	}

	public void setD3(List<Device> d3) {
		this.d3 = d3;
	}

	public List<Device> getMedia() {
		return media;
	}

	public void setMedia(List<Device> media) {
		this.media = media;
	}

	@Override
	public String toString() {
		return "CostsGraphs [d1=" + d1 + ", d2=" + d2 + ", d3=" + d3 + ", media=" + media + "]";
	}
	
}
