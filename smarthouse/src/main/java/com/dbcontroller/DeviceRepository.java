package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;

import com.models.Device;

public interface DeviceRepository extends CrudRepository<Device, Integer>{

}
