package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;

import com.models.User;


public interface UserRepository extends CrudRepository<User, String>{
	
}
