package com.dbcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.models.Cost;
import com.models.DivisionCost;
import com.models.DivisionData;
import com.models.HouseDivision;
import com.restclient.CallRestService;

@Component
@ComponentScan({"com.restclient"})
@EnableScheduling
public class MainController {
	@Autowired
	CallRestService restService;
	
	@Autowired
	CO2Repository co2Repository;
	
	@Autowired
	DivisionRepository divisionRepository;
	
	@Autowired
	HumidityRepository humidityRepository;
	
	@Autowired
	LightsRepository lightsRepository;
	
	@Autowired
	MovementRepository movementRepository;
	
	@Autowired
	TemperatureRepository temperatureRepository;
	
	@Autowired
	DeviceRepository deviceRepository;
	
	@Autowired
	DivisionCostRepository divisionCostRepository;
	
	
	/////////////////////////////////////////////////////////////ATUALIZAR BD//////////////////////////////////////////////////////////
	@Scheduled(fixedRate=3600000*2)
	public void addDivisions() {
		List<HouseDivision> divisions = restService.getDivisions();
		if(divisions!= null) {
			for(int i=0; i < divisions.size(); i++) {							
				DivisionData division = new DivisionData(divisions.get(i).time, divisions.get(i).idDivision, divisions.get(i).co2.value, divisions.get(i).humidity.value, divisions.get(i).light.value, divisions.get(i).movement.value, divisions.get(i).temperature.value);
				System.out.println(division);
				divisionRepository.save(division);
				temperatureRepository.save(divisions.get(i).temperature);
				co2Repository.save(divisions.get(i).co2);
				humidityRepository.save(divisions.get(i).humidity);
				lightsRepository.save(divisions.get(i).light);
				movementRepository.save(divisions.get(i).movement);
			}
			System.out.println("Adicionado com sucesso!");
		}
		
	}
	
	@Scheduled(fixedRate=3600000*2)
	public void addCosts() {
		List<Cost> costs = restService.getCosts();
		System.out.println(costs);
		if(costs != null) {
			for(int i=0; i<costs.size(); i++) {
				DivisionCost costDivision = new DivisionCost(costs.get(i).d1.time, costs.get(i).idDivisao, costs.get(i).d1.name, costs.get(i).d1.value, costs.get(i).d2.name, costs.get(i).d2.value, costs.get(i).d3.name, costs.get(i).d3.value);
				divisionCostRepository.save(costDivision);
				deviceRepository.save(costs.get(i).d1);
				deviceRepository.save(costs.get(i).d2);
				deviceRepository.save(costs.get(i).d3);
			}
			System.out.println("Adicionado com sucesso!");
		}
		
	}
}
