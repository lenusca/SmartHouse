package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.models.TemperatureSensor;

@Repository
public interface TemperatureRepository extends CrudRepository<TemperatureSensor, Integer>{

}
