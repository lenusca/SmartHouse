package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;

import com.models.DivisionCost;

public interface DivisionCostRepository extends CrudRepository<DivisionCost, Integer>{

}
