package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.models.HumiditySensor;

@Repository
public interface HumidityRepository extends CrudRepository<HumiditySensor, Integer>{

}
