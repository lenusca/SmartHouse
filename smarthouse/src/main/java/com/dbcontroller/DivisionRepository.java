package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.models.DivisionData;


@Repository
public interface DivisionRepository extends CrudRepository<DivisionData, String>{

}
