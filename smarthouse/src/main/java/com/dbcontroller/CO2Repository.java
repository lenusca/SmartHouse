package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.models.CO2Sensor;

@Repository
public interface CO2Repository extends CrudRepository<CO2Sensor, Integer>{

}
