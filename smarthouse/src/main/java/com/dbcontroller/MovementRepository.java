package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.models.MovementSensor;

@Repository
public interface MovementRepository extends CrudRepository<MovementSensor, Integer>{

}
