package com.dbcontroller;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.models.LightsSensor;

@Repository
public interface LightsRepository extends CrudRepository<LightsSensor, Integer>{

}
