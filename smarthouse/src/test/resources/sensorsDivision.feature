Feature: Check the values of the CO2, temperature, light, humidity and movement sensors, in a given room

    Scenario Outline: The user wants to check the values of the CO2, temperature, light, humidity and movement sensors, in the '<room>'
        Given I am at the web app
        When I select the <room> with humidity of <humidity> and temperature of <temperature>
        Then The values of the sensors in the <room> appear on the screen

    Examples: 
        | room    | humidity | temperature |
        | quarto  | 80       | 25          |
