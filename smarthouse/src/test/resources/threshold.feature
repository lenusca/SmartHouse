Feature: Keep the temperature and/or the humidity within a certain threshold, in a given room

    Scenario Outline: The user wants to adjust the value of the temperature sensor within <temperatureMax> and <temperatureMin>, in the <room>
        Given I selected the temperature sensor on the <room>
        And <threshold> the threshold
        And Selected the <temperatureMax> and <temperatureMin> as threshold
        When The temperature is <temperature>
        Then The AC should be turned <expectedAC>

    Examples:
        | room    | threshold  | temperatureMax | temperatureMin | temperature | expectedAC |
        | quarto  | allowed    | 25             | 10             | 20          | off        |
        | cozinha | allowed    | 20             | 15             | 12          | on         |
        | cozinha | allowed    |                |                | 12          | off        |
        | cozinha | disallowed | 20             | 15             | 10          | off        |
        | wc      | disallowed |                |                | 10          | off        |