Feature: 	Movement notification

    Scenario Outline: The user wants to receive a notification alerting him that there is movement on his residence
        Given a movement sensor <movementSensor>
        And configuration to sent notification to <email> when motion detection
        When Movement <movementDetected> detected on the sensor
        Then A notification <notificationSend> be sent

    Examples:
        | movementSensor | email     | movementDetected | notificationSend |
        | on             | tmp@ua.pt | is               | should           |
        | on             | tmp@ua.pt | is not           | should not       |
        | on             |           | is               | should not       |
        | off            | tmp@ua.pt | is               | should not       |
        | off            |           | is               | should not       |