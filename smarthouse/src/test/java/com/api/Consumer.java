package com.api;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;


public class Consumer
{
    private static final int MAX_ALLOWED_LATENCY = 5000;

    private final Properties properties = getProperties();

    private final String topic;

    public Consumer( String topic )
    {
        this.topic = topic;
    }

    public String consume()
    {
    	String out = "";
    	ConsumerRecords<String, String> consumerRecords;
        try (KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(properties)) {
            kafkaConsumer.subscribe(Collections.singletonList(topic));

            long endPollingTimestamp = System.currentTimeMillis() + MAX_ALLOWED_LATENCY;

            
            while ( System.currentTimeMillis() < endPollingTimestamp ) {
            		consumerRecords = kafkaConsumer.poll(100);
                for ( ConsumerRecord<String, String> next : consumerRecords ) {
                    out += next;
                }
            }
            
               

        }
        return out;
    }

    private Properties getProperties()
    {
    	Properties result = new Properties();
         
        result.put("bootstrap.servers", "PLAINTEXT://192.168.160.103:9092");
        result.put("group.id", "test-group");

        // add consumer specific result
        result.setProperty("enable.auto.commit", "true");
        result.setProperty("auto.commit.interval.ms", "1000");
        result.setProperty("session.timeout.ms", "30000");
        result.setProperty("metadata.max.age.ms", "1000");
        result.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        result.setProperty("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        return result;
    }
}