//package com.api;
//
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.clients.consumer.ConsumerRecords;
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.clients.producer.ProducerRecord;
//import org.apache.kafka.clients.producer.RecordMetadata;
//import org.apache.kafka.common.serialization.Serdes;
//import org.apache.kafka.common.serialization.StringDeserializer;
//import org.apache.kafka.common.serialization.StringSerializer;
//import org.apache.kafka.streams.StreamsConfig;
//import org.apache.kafka.streams.Topology;
//import org.apache.kafka.streams.TopologyTestDriver;
//import org.apache.kafka.streams.test.ConsumerRecordFactory;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;
//import org.junit.ClassRule;
//import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import static org.awaitility.Awaitility.await;
//import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
//
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//import java.util.UUID;
//import java.util.concurrent.ExecutionException;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.TimeoutException;
//import java.time.Duration;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.HashMap;
//
//import com.models.DivisionData;
//import com.restclient.CallRestService;
//import com.models.HouseDivision;
//
//
//import static org.junit.Assert.*;
//
//public class StepDefs {
//
//    public static WebDriver driver;
//    String baseUrl = "http://localhost:8888";
//
//    //Test for movement
//    private Boolean toSend;
//    private Boolean movementDetected;
//    private String email = "";
//
//    private List<DivisionData> listdivisions = new ArrayList<DivisionData>();
//    
//
//    private static final String TOPIC = "test-topic";
//
//    private final Producer producer_ = new Producer();
//
//    private final Consumer consumer_ = new Consumer(TOPIC);
//
//    
//    @Given("^a movement sensor (.*?)$")
//    public void movementSensorAOn(String movementSensor){
//        //verificar se o sensor de movimento está ativo
//        toSend = movementSensor.contains("off") ? false : true;  
//      //selenium para entrar no site
//        //path para linux 64 bits. https://github.com/mozilla/geckodriver/releases para download de outras versões
//         System.setProperty("webdriver.gecko.driver","src/test/java/com/api/geckodriver2");
//         driver = new FirefoxDriver();
//         driver.manage().window().maximize();
//         driver.get(baseUrl);
//    }
//
//    @And("^configuration to sent notification to (.*?) when motion detection$")
//    public void configuration(String email){
//        //verificar se tem um email configurado
//        this.email = email;
//    }
//
//    @When("^Movement (.*?) detected on the sensor$")
//    public void movementDetected(String movementDetected){
//    	 //enviar pelo kafka a dizer que foi detetado movimento
//    	String sendInfo;
//    	if(movementDetected.contains("not")) {
//    		sendInfo = "{\"idDivision\": \"cozinha\", \"time\": \"2020-05-31 18:43:00.346654\", \"temperature\": {\"value\": 17, \"time\": \"2020-05-31 18:43:00.346654\"}, \"co2\": {\"value\": 315, \"time\": \"2020-05-31 18:43:00.346654\"}, \"humidity\": {\"value\": 45, \"time\": \"2020-05-31 18:43:00.346654\"}, \"light\": {\"value\": false, \"time\": \"2020-05-31 18:43:00.346654\"}, \"movement\": {\"value\": false, \"time\": \"2020-05-31 18:43:00.346654\"}}";
//    	}
//    	else {
//    		sendInfo = "{\"idDivision\": \"cozinha\", \"time\": \"2020-05-31 18:43:00.346654\", \"temperature\": {\"value\": 17, \"time\": \"2020-05-31 18:43:00.346654\"}, \"co2\": {\"value\": 315, \"time\": \"2020-05-31 18:43:00.346654\"}, \"humidity\": {\"value\": 45, \"time\": \"2020-05-31 18:43:00.346654\"}, \"light\": {\"value\": false, \"time\": \"2020-05-31 18:43:00.346654\"}, \"movement\": {\"value\": true, \"time\": \"2020-05-31 18:43:00.346654\"}}";
//    	}
//    	producer_.emit(TOPIC,sendInfo);
//    	driver.get("/DivisionInfo/?idDivision=cozinha");
//    	String consumed = driver.findElement(By.xpath("/html/body/pre/")).getText();
//    	JSONParser parser = new JSONParser();
//    	JSONObject json = new JSONObject();
//    	try {
//			json =  (JSONObject) parser.parse(consumed);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    	consumed = (String) json.get("movement");
//    	this.movementDetected =consumed.contains("is not") ? false : true;
//    }
//
//    @Then("^A notification (.*?) be sent$")
//    public void notificationSent(String notificationSend){
//        //verificar se o email é produzido e enviado (não é necessário enviar o e-mail)
//        if(toSend == true && movementDetected == true && email.length() > 2){
//            System.out.println("Notification sent to " + email);
//            assertFalse(notificationSend.contains("should not"));
//        }
//        else{
//            if(movementDetected == true && email.length() > 2){
//                System.out.println("No notification sent to " + email + " because the notifications are not turned on");
//            }
//            else if(movementDetected == false && email.length() > 2){
//                System.out.println("No notification sent to " + email + " because there is no movement detected");
//            }
//            else{
//                System.out.println("Impossible to send notification, because no email was inserted");
//            }
//            assertTrue(notificationSend.contains("should not"));
//        }
//    }
//
//    //Test for sensors in a division
//    private String room;
//
//    private int humidity = 0;
//    private int temperature = 0;
//
//    private int humidityRead = 0;
//    private int temperatureRead = 0;
//
//    @Given("I am at the web app")
//    public void atWebApp(){
//        //selenium para entrar no site
//        //path para linux 64 bits. https://github.com/mozilla/geckodriver/releases para download de outras versões
//        // System.setProperty("webdriver.gecko.driver","src/test/java/com/api/geckodriver");
//        // driver = new FirefoxDriver();
//        // driver.manage().window().maximize();
//        // driver.get(baseUrl);
//    }
//
//    @When("^I select the (.*?) with humidity of (.*?) and temperature of (.*?)$")
//    public void selectRoom(String room, String humidity, String temperature){
//        try{
//            this.temperature = Integer.parseInt(temperature);
//            this.humidity = Integer.parseInt(humidity);
//        }
//        catch(NumberFormatException e){
//            this.temperature = 0;
//            this.humidity = 0;
//        }
//        this.room = room;
//
//        //mandar os valores da humidade e temperatura de uma divisão por kafka
//
//
//        //abrir a divisão no site
//        //encontrar o elemento da divisão que se procura
//        //WebElement roomSite = driver.findElement(By.id(room));
//        //carregar no elemento para se abrir a divisão no site
//        //roomSite.click();
//
//    }
//
//    @Then("The values of the sensors in the (.*?) appear on the screen")
//    public void sensorsValue(String room){
//        //ler os valores no site 
//
//        //comparar com os enviados no teste
//        // assertEquals(humidity, humidityRead);
//        // assertEquals(temperature, temperatureRead);
//
//        // driver.quit();
//    }
//
//    //Test for threshold
//    private String roomThreshold;
//    private Boolean threshHold;
//    private Boolean threshHoldMissConfig = false;
//    private int tempMax, tempMin;
//    private boolean thresholdWithiness;
//
//    @Given("^I selected the temperature sensor on the (.*?)$")
//    public void selectedTemperatureSensorOnRoom(String roomThreshold){
//        //selecionar o sensor de temperatura na divisão
//        this.roomThreshold = roomThreshold;
//    }
//
//    @And("(.*?) the threshold")
//    public void allowedThreshold(String threshHold){
//        //permitir, ou não, os thresholds
//        this.threshHold = threshHold.contains("disallowed") ? false :  true;
//    }
//
//    @And("^Selected the (.*?) and (.*?) as threshold$")
//    public void selectedThreshold(String tempMax, String tempMin){
//        //selecionar os valores do threshold para a divisão
//        // this.tempMax = tempMax;
//        // this.tempMin = tempMin;      
//        try{
//            this.tempMax = Integer.parseInt(tempMax);
//            this.tempMin = Integer.parseInt(tempMin);
//        }
//        catch(NumberFormatException e){
//            this.tempMax = 0;
//            this.tempMin = 0;
//            this.threshHoldMissConfig = true;
//        }
//    }
//
//    @When("^The temperature is (.*?)$")
//    public void withinThreshold(String temperature){
//        //mandar uma temperatura via kafka que esteja dentro/fora do threshold
//        int tempThresh;
//        try{
//            tempThresh = Integer.parseInt(temperature);
//        }
//        catch(NumberFormatException e){
//            tempThresh = 0;
//        }
//        this.thresholdWithiness = (tempThresh <= tempMax && tempThresh >= tempMin && threshHold == true) ? true : false;
//    }
//
//    
//    @Then("^The AC should be turned (.*?)$")
//    public void acState(String expectedAC){
//        //verificar se o sistema responde ao valor que esteja fora do threshold, se os threshold estiverem bem definidos
//        //não fazer nada caso contrário.
//        if(threshHoldMissConfig == true){
//            System.out.println("AC not turned on, in the " + roomThreshold + ", because the threshold is not configured correctly!");
//            assertEquals(expectedAC, "off");
//        }
//        else if(thresholdWithiness == true){
//            System.out.println("AC not turned on, in the " + roomThreshold + ", because it is inside the threshold");
//            assertEquals(expectedAC, "off");
//        }
//        else{
//            if(threshHold == true){
//                System.out.println("AC turned on, in the " + roomThreshold);
//                assertEquals(expectedAC, "on");
//            }
//            else{
//                System.out.println("AC not turned on, in the " + roomThreshold + ", because the threshold is not allowed");
//                assertEquals(expectedAC, "off");
//            }
//        }
//    }
//    
//   
//    //Test kafka producer
//    @ClassRule
//    private EmbeddedSingleNodeKafkaCluster CLUSTER = new EmbeddedSingleNodeKafkaCluster();
//    
//    KafkaProducer<String, String> producer;
//    private String message;
//    private String topic;
//    private  RecordMetadata recordMetadata;
//    
//    @Given("^a kafka pipeline with a (.*?)$")
//    public void startPipeline(String topic) throws Exception {
//    	CLUSTER.start();
//    	producer = new KafkaProducer<>(getProducerProperties());
//    	this.topic = topic;
//    	assertTrue(CLUSTER.isRunning());
//    	CLUSTER.createTopic(topic);
//   
//    }
//    @And("^message to be sent (.*?)$")
//    public void prepareMessage(String message) {
//    	this.message = message;
//    }
//    @When("message is sent")
//    public void sendMessage() throws InterruptedException, ExecutionException, TimeoutException {
//    	// async with callback
//        producer.send(
//            new ProducerRecord<>(topic, "k1", "v1"),
//            ((metadata, exception) -> {
//              if (exception != null) fail();
//            }));
//
//        // sync
//         recordMetadata =
//            producer.send(new ProducerRecord<>(topic, "k2", "v2")).get(3, TimeUnit.SECONDS);
//    }
//    @Then("message produced should have timestamp")
//    public void assertReceived() {
//    	 assertTrue(recordMetadata.hasOffset());
//    	 assertTrue(recordMetadata.hasTimestamp());
//    	 if(recordMetadata.hasOffset() && recordMetadata.hasTimestamp()) {
//    		 System.out.println("Kafka produced message correcty");
//    		 assert(recordMetadata.hasOffset());
//    		 assert(recordMetadata.hasTimestamp());
//    	 }
//    	 else {
//    		 System.out.println("Kafka failed to produce message correcty");
//    	 }
//    }
//    
//    //Test kafka consumer
//    
//    String ConsumerTopic;
//    String message1;
//    String message2;
//    KafkaConsumer<String, String> consumer;
//    
//    
//    @Given("^a kafka consumer pipeline with a (.*?)$")
//    public void setTopic(String topic) throws Exception {
//    	CLUSTER.start();
//    	assertTrue(CLUSTER.isRunning());
//    	this.ConsumerTopic = topic;
//    	CLUSTER.createTopic(topic);
//    	producer = new KafkaProducer<>(getProducerProperties());
//    	consumer = new KafkaConsumer<>(getConsumerProperties());
//    	producer.send(new ProducerRecord<>(topic, "k3", "v3")).get(1, TimeUnit.SECONDS);
//        
//    }
//    @And("^message to be received (.*?)$")
//    public void setMessage(String message1) {
//    	this.message1= message1;
//    	consumer.subscribe(Collections.singletonList(ConsumerTopic));
//    }
//    @When("^a (.*?) is sent$")
//    public void sendM(String message2) {
//    	this.message2 = message2;
//    }
//    @Then("message should be received on the other end") //Verificar se API responde de forma adequada
//    public void assertEqualsM() {
//    	final ArrayList<String> values = new ArrayList<>();
//        await()
//            .atMost(15, TimeUnit.SECONDS)
//            .untilAsserted(
//                () -> {
//                  final ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));
//                  for (final ConsumerRecord<String, String> record : records)
//                    values.add(record.value());
//                  assertEquals(1, values.size());
//                  
//                });
//        if(1==values.size()) {
//        	System.out.println("Kafka Consumed message correctly");
//        	assertEquals(1,values.size());
//        }
//        else {
//        	System.out.println("Kafka failed consuming message");
//        }
//    }
//    
//	private Properties getProducerProperties() {
//	    final Properties properties = new Properties();
//	    properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
//	    properties.put(ProducerConfig.CLIENT_ID_CONFIG, UUID.randomUUID().toString());
//	    properties.put(ProducerConfig.ACKS_CONFIG, "all");
//	    properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//	    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//	    return properties;
//	  }
//
//	private Properties getConsumerProperties() {
//	    final Properties properties = new Properties();
//	    properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
//	    properties.put(ConsumerConfig.CLIENT_ID_CONFIG, "client0");
//	    properties.put(ConsumerConfig.GROUP_ID_CONFIG, "group0");
//	    properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//	    properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//	    properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//	    return properties;
//	  }
//	
//	
////Test kafka unit
//    
//	private  String topicIn;
//	private  String topicOut;
//	private TopologyTestDriver testDriver;
//	private Properties properties;
//	private ProducerRecord<String, String> outRecord1;
//	private ProducerRecord<String, String> outRecord2;
//	private ProducerRecord<String, String> outRecord3;
//    private String m1;
//    private String m2;
//	
//	
//    @Given("^a kafka pipeline with topics (.*?) and (.*?)$")
//    public void setTopicInOut(String topicIn, String topicOut) throws Exception {
//    	this.topicIn = topicIn;
//    	this.topicOut = topicOut;
//        
//    }
//    @And("^proporties defined as")
//    public void setProperties() {
//    	properties = new Properties();
//        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
//        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
//        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
//        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
//    }
//    	
//    @When("^records (.*?) and (.*?) are produced$")
//    public void sendM(String m1, String m2) {
//    	this.m1 = m1;
//    	this.m2 = m2;
//    	// Arrange
//        final Topology topology = StreamProcessing.topologyUpperCase(topicIn, topicOut);
//        testDriver = new TopologyTestDriver(topology, properties);
//        final ConsumerRecordFactory<String, String> factory =
//            new ConsumerRecordFactory<>(topicIn, new StringSerializer(), new StringSerializer());
//        final ConsumerRecord<byte[], byte[]> record1 = factory.create(topicIn, null, m1);
//        final ConsumerRecord<byte[], byte[]> record2 = factory.create(topicIn, null, m2);
//
//        // Act
//        testDriver.pipeInput(Arrays.asList(record1, record2));
//        outRecord1 = testDriver.readOutput(topicOut, new StringDeserializer(), new StringDeserializer());
//        outRecord2 = testDriver.readOutput(topicOut, new StringDeserializer(), new StringDeserializer());
//        outRecord3 = testDriver.readOutput(topicOut, new StringDeserializer(), new StringDeserializer());
//    }
//    @Then("records should match the input")
//    public void assertRecordsMatch() {
//    	// Assert
//     
//        if(this.m1.toUpperCase().equalsIgnoreCase(outRecord1.value().toUpperCase())) {
//            System.out.println("outRecord1 is not NULL and equals " + outRecord1.value());
//            assertEquals(this.m1.toUpperCase(), outRecord1.value());
//        }
//        if(this.m2.toUpperCase().equalsIgnoreCase(outRecord2.value().toUpperCase())) {
//            System.out.println("outRecord2 is not NULL and equals " + outRecord2.value());
//            assertEquals(this.m2.toUpperCase(), outRecord2.value());
//        }
//        
//    }
//    
//}

package com.api;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.ClassRule;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.awaitility.Awaitility.await;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import com.models.DivisionData;
import com.restclient.CallRestService;
import com.models.HouseDivision;
import static org.junit.Assert.*;
public class StepDefs {
    public static WebDriver driver;
    String baseUrl = "http://localhost:3000";
    //Test for movement
    private Boolean toSend;
    private Boolean movementDetected;
    private String email = "";
    private List<DivisionData> listdivisions = new ArrayList<DivisionData>();
    
    private static final String TOPIC = "test-topic";
    private final Producer producer_ = new Producer();
    private final Consumer consumer_ = new Consumer(TOPIC);
    
    @Given("^a movement sensor (.*?)$")
    public void movementSensorAOn(String movementSensor){
        //verificar se o sensor de movimento está ativo
        toSend = movementSensor.contains("off") ? false : true;     
    }
    @And("^configuration to sent notification to (.*?) when motion detection$")
    public void configuration(String email){
        //verificar se tem um email configurado
        this.email = email;
    }
    @When("^Movement (.*?) detected on the sensor$")
    public void movementDetected(String movementDetected){
             //enviar pelo kafka a dizer que foi detetado movimento
            producer_.emit(TOPIC,movementDetected);
        String consumed = consumer_.consume();
            this.movementDetected = consumed.contains("is not") ? false : true;
    }
    @Then("^A notification (.*?) be sent$")
    public void notificationSent(String notificationSend){
        //verificar se o email é produzido e enviado (não é necessário enviar o e-mail)
        if(toSend == true && movementDetected == true && email.length() > 2){
            System.out.println("Notification sent to " + email);
            assertFalse(notificationSend.contains("should not"));
        }
        else{
            if(movementDetected == true && email.length() > 2){
                System.out.println("No notification sent to " + email + " because the notifications are not turned on");
            }
            else if(movementDetected == false && email.length() > 2){
                System.out.println("No notification sent to " + email + " because there is no movement detected");
            }
            else{
                System.out.println("Impossible to send notification, because no email was inserted");
            }
            assertTrue(notificationSend.contains("should not"));
        }
    }
    //Test for sensors in a division
    private String room;
    private int humidity = 0;
    private int temperature = 0;
    private int humidityRead = 0;
    private int temperatureRead = 0;
    @Given("I am at the web app")
    public void atWebApp(){
        //selenium para entrar no site
        //path para linux 64 bits. https://github.com/mozilla/geckodriver/releases para download de outras versões
        // System.setProperty("webdriver.gecko.driver","src/test/java/com/api/geckodriver");
        // driver = new FirefoxDriver();
        // driver.manage().window().maximize();
        // driver.get(baseUrl);
    }
    @When("^I select the (.*?) with humidity of (.*?) and temperature of (.*?)$")
    public void selectRoom(String room, String humidity, String temperature){
        try{
            this.temperature = Integer.parseInt(temperature);
            this.humidity = Integer.parseInt(humidity);
        }
        catch(NumberFormatException e){
            this.temperature = 0;
            this.humidity = 0;
        }
        this.room = room;
        //mandar os valores da humidade e temperatura de uma divisão por kafka
        //abrir a divisão no site
        //encontrar o elemento da divisão que se procura
        //WebElement roomSite = driver.findElement(By.id(room));
        //carregar no elemento para se abrir a divisão no site
        //roomSite.click();
    }
    @Then("The values of the sensors in the (.*?) appear on the screen")
    public void sensorsValue(String room){
        //ler os valores no site 
        //comparar com os enviados no teste
        // assertEquals(humidity, humidityRead);
        // assertEquals(temperature, temperatureRead);
        // driver.quit();
    }
    //Test for threshold
    private String roomThreshold;
    private Boolean threshHold;
    private Boolean threshHoldMissConfig = false;
    private int tempMax, tempMin;
    private boolean thresholdWithiness;
    @Given("^I selected the temperature sensor on the (.*?)$")
    public void selectedTemperatureSensorOnRoom(String roomThreshold){
        //selecionar o sensor de temperatura na divisão
        this.roomThreshold = roomThreshold;
    }
    @And("(.*?) the threshold")
    public void allowedThreshold(String threshHold){
        //permitir, ou não, os thresholds
        this.threshHold = threshHold.contains("disallowed") ? false :  true;
    }
    @And("^Selected the (.*?) and (.*?) as threshold$")
    public void selectedThreshold(String tempMax, String tempMin){
        //selecionar os valores do threshold para a divisão
        // this.tempMax = tempMax;
        // this.tempMin = tempMin;      
        try{
            this.tempMax = Integer.parseInt(tempMax);
            this.tempMin = Integer.parseInt(tempMin);
        }
        catch(NumberFormatException e){
            this.tempMax = 0;
            this.tempMin = 0;
            this.threshHoldMissConfig = true;
        }
    }
    @When("^The temperature is (.*?)$")
    public void withinThreshold(String temperature){
        //mandar uma temperatura via kafka que esteja dentro/fora do threshold
        int tempThresh;
        try{
            tempThresh = Integer.parseInt(temperature);
        }
        catch(NumberFormatException e){
            tempThresh = 0;
        }
        this.thresholdWithiness = (tempThresh <= tempMax && tempThresh >= tempMin && threshHold == true) ? true : false;
    }
    
    @Then("^The AC should be turned (.*?)$")
    public void acState(String expectedAC){
        //verificar se o sistema responde ao valor que esteja fora do threshold, se os threshold estiverem bem definidos
        //não fazer nada caso contrário.
        if(threshHoldMissConfig == true){
            System.out.println("AC not turned on, in the " + roomThreshold + ", because the threshold is not configured correctly!");
            assertEquals(expectedAC, "off");
        }
        else if(thresholdWithiness == true){
            System.out.println("AC not turned on, in the " + roomThreshold + ", because it is inside the threshold");
            assertEquals(expectedAC, "off");
        }
        else{
            if(threshHold == true){
                System.out.println("AC turned on, in the " + roomThreshold);
                assertEquals(expectedAC, "on");
            }
            else{
                System.out.println("AC not turned on, in the " + roomThreshold + ", because the threshold is not allowed");
                assertEquals(expectedAC, "off");
            }
        }
    }
    
   
    //Test kafka producer
    @ClassRule
    private EmbeddedSingleNodeKafkaCluster CLUSTER = new EmbeddedSingleNodeKafkaCluster();
    
    KafkaProducer<String, String> producer;
    private String message;
    private String topic;
    private  RecordMetadata recordMetadata;
    
    @Given("^a kafka pipeline with a (.*?)$")
    public void startPipeline(String topic) throws Exception {
            CLUSTER.start();
            producer = new KafkaProducer<>(getProducerProperties());
            this.topic = topic;
            assertTrue(CLUSTER.isRunning());
            CLUSTER.createTopic(topic);
   
    }
    @And("^message to be sent (.*?)$")
    public void prepareMessage(String message) {
            this.message = message;
    }
    @When("message is sent")
    public void sendMessage() throws InterruptedException, ExecutionException, TimeoutException {
            // async with callback
        producer.send(
            new ProducerRecord<>(topic, "k1", "v1"),
            ((metadata, exception) -> {
              if (exception != null) fail();
            }));
        // sync
         recordMetadata =
            producer.send(new ProducerRecord<>(topic, "k2", "v2")).get(3, TimeUnit.SECONDS);
    }
    @Then("message produced should have timestamp")
    public void assertReceived() {
             assertTrue(recordMetadata.hasOffset());
             assertTrue(recordMetadata.hasTimestamp());
             if(recordMetadata.hasOffset() && recordMetadata.hasTimestamp()) {
                     System.out.println("Kafka produced message correcty");
                     assert(recordMetadata.hasOffset());
                     assert(recordMetadata.hasTimestamp());
             }
             else {
                     System.out.println("Kafka failed to produce message correcty");
             }
    }
    
    //Test kafka consumer
    
    String ConsumerTopic;
    String message1;
    String message2;
    KafkaConsumer<String, String> consumer;
    
    
    @Given("^a kafka consumer pipeline with a (.*?)$")
    public void setTopic(String topic) throws Exception {
            CLUSTER.start();
            assertTrue(CLUSTER.isRunning());
            this.ConsumerTopic = topic;
            CLUSTER.createTopic(topic);
            producer = new KafkaProducer<>(getProducerProperties());
            consumer = new KafkaConsumer<>(getConsumerProperties());
            producer.send(new ProducerRecord<>(topic, "k3", "v3")).get(1, TimeUnit.SECONDS);
        
    }
    @And("^message to be received (.*?)$")
    public void setMessage(String message1) {
            this.message1= message1;
            consumer.subscribe(Collections.singletonList(ConsumerTopic));
    }
    @When("^a (.*?) is sent$")
    public void sendM(String message2) {
            this.message2 = message2;
    }
    @Then("message should be received on the other end") //Verificar se API responde de forma adequada
    public void assertEqualsM() {
            final ArrayList<String> values = new ArrayList<>();
        await()
            .atMost(15, TimeUnit.SECONDS)
            .untilAsserted(
                () -> {
                  final ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));
                  for (final ConsumerRecord<String, String> record : records)
                    values.add(record.value());
                  assertEquals(1, values.size());
                  
                });
        if(1==values.size()) {
                System.out.println("Kafka Consumed message correctly");
                assertEquals(1,values.size());
        }
        else {
                System.out.println("Kafka failed consuming message");
        }
    }
    
        private Properties getProducerProperties() {
            final Properties properties = new Properties();
            properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
            properties.put(ProducerConfig.CLIENT_ID_CONFIG, UUID.randomUUID().toString());
            properties.put(ProducerConfig.ACKS_CONFIG, "all");
            properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
            properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
            return properties;
          }
        private Properties getConsumerProperties() {
            final Properties properties = new Properties();
            properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, CLUSTER.bootstrapServers());
            properties.put(ConsumerConfig.CLIENT_ID_CONFIG, "client0");
            properties.put(ConsumerConfig.GROUP_ID_CONFIG, "group0");
            properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
            properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
            return properties;
          }
        
        
//Test kafka unit
    
        private  String topicIn;
        private  String topicOut;
        private TopologyTestDriver testDriver;
        private Properties properties;
        private ProducerRecord<String, String> outRecord1;
        private ProducerRecord<String, String> outRecord2;
        private ProducerRecord<String, String> outRecord3;
    private String m1;
    private String m2;
        
        
    @Given("^a kafka pipeline with topics (.*?) and (.*?)$")
    public void setTopicInOut(String topicIn, String topicOut) throws Exception {
            this.topicIn = topicIn;
            this.topicOut = topicOut;
        
    }
    @And("^proporties defined as")
    public void setProperties() {
            properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
    }
            
    @When("^records (.*?) and (.*?) are produced$")
    public void sendM(String m1, String m2) {
            this.m1 = m1;
            this.m2 = m2;
            // Arrange
        final Topology topology = StreamProcessing.topologyUpperCase(topicIn, topicOut);
        testDriver = new TopologyTestDriver(topology, properties);
        final ConsumerRecordFactory<String, String> factory =
            new ConsumerRecordFactory<>(topicIn, new StringSerializer(), new StringSerializer());
        final ConsumerRecord<byte[], byte[]> record1 = factory.create(topicIn, null, m1);
        final ConsumerRecord<byte[], byte[]> record2 = factory.create(topicIn, null, m2);
        // Act
        testDriver.pipeInput(Arrays.asList(record1, record2));
        outRecord1 = testDriver.readOutput(topicOut, new StringDeserializer(), new StringDeserializer());
        outRecord2 = testDriver.readOutput(topicOut, new StringDeserializer(), new StringDeserializer());
        outRecord3 = testDriver.readOutput(topicOut, new StringDeserializer(), new StringDeserializer());
    }
    @Then("records should match the input")
    public void assertRecordsMatch() {
            // Assert
     
        if(this.m1.toUpperCase().equalsIgnoreCase(outRecord1.value().toUpperCase())) {
            System.out.println("outRecord1 is not NULL and equals " + outRecord1.value());
            assertEquals(this.m1.toUpperCase(), outRecord1.value());
        }
        if(this.m2.toUpperCase().equalsIgnoreCase(outRecord2.value().toUpperCase())) {
            System.out.println("outRecord2 is not NULL and equals " + outRecord2.value());
            assertEquals(this.m2.toUpperCase(), outRecord2.value());
        }
        
    }
    
}