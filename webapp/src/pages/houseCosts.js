import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { Line } from 'react-chartjs-2';

class HouseCosts extends Component{
    constructor(){
        super();
        var today = new Date();
    
        this.state = {
            loading : true,
            dataHouse: [],
            startDate : today,
            graph: [],
            devices: {
                labels: [],
                datasets: [
                  {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(246,78,8, 0.4)',
                    borderColor: 'rgba(246,78,8, 1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(246,78,8, 1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(246,78,8, 1)',
                    pointHoverBorderColor: 'rgba(246,78,8, 1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  },
                  {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(55,118,255, 0.4)',
                    borderColor: 'rgba(55,118,255, 1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(55,118,255, 1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(55,118,255, 1)',
                    pointHoverBorderColor: 'rgba(55,118,255, 1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  },
                  {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(246,79,102,0.4)',
                    borderColor: 'rgba(246,79,102,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(246,79,102,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(246,79,102,1)',
                    pointHoverBorderColor: 'rgba(246,79,102,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  },
                  {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(52,186,2, 0.4)',
                    borderColor: 'rgba(52,186,2,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(52,186,2,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(52,186,2,1)',
                    pointHoverBorderColor: 'rgba(52,186,2,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  },
                  {
                    label: 'House',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(49,36,80, 0.4)',
                    borderColor: 'rgba(49,36,80,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(49,36,80,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(49,36,80,1)',
                    pointHoverBorderColor: 'rgba(49,36,80,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  }

                ]
              }                         
        }       
    }

    componentDidMount(){
        console.log(new Date().now);
    
        fetch("http://192.168.160.103:32080/CostHouse/").then(response=> response.json()).then(data => this.setState({dataHouse: data, isLoading:false}))
    }

    /* mudar a data yyyy/MM/DD */
    setStartDate(startDate){
        var month = startDate.getMonth()+1;
        var day = startDate.getDate();
        
        /*Para colocar 2 digitos*/ 
        if(month.toString().length === 1)
        {
            month = "0"+month; 
        }
        if(day.toString().length ===1){
            day = "0"+day;
        }
        
        var date = startDate.getFullYear()+"/"+month+"/"+day;
    
        fetch("http://192.168.160.103:32080/GraphHouseCost/?time="+date).then(response=> response.json()).then(data => this.setState({graph: data, isLoading:false, startDate: startDate}))        
    }
  

    render (){
        return(
            <div>
                
                <section id="sidebar">
			
                    <div id="logo">
                        <img src={"images/logo.png"} height="240px" alt=""/> 
                    </div>	
                    
                    <div class="inner">
                        <nav>
                            <ul>	
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}><a href="/">Divisions</a></li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/House">House &#9662;</a>
                                    <ul class="dropdown">
                                        <li ><a href="/House">Sensors Value</a></li>
                                        <li><a href="/HouseCosts">Costs</a></li>
                                    </ul>
                                    
                                </li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/EditSensors">Edit Sensors &#9662;</a>
                                    <ul class="dropdown" style={{marginBottom:"0% !important"}}>
                                        <li><a href="/EditHouseSensors">House</a></li>
                                        <li><a href="/EditDivision">Division</a></li>
                                    </ul>
                                    
                                </li>      
                            </ul>
                            <ul style={{position:"relative", top:"170px", marginBottom:"0%", justifyContent:"center !important", alignContent:"center"}}>
                                <li style={{display: "inline-block"}}><a href="/Login"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                            </ul>
                        </nav>
                </div>
                </section>

                { this.state.dataHouse.length === 0?(
                    <div id="wrapper" style={{backgroundColor:"white", height:"980px"}}>
                        <div style={{marginLeft:"35%", backgroundColor:"white"}}>
                            <iframe src="https://giphy.com/embed/1wrRQfA0DcS7n3zaX0"  height="500px" width="500px" style={{marginTop:"20%"}} title = "TEST"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
                        </div>
                    </div>
                ):(
                    <div id="wrapper" >
                    <div style={{marginLeft: "5%", marginTop: "2%", marginBottom: "-20px"}}>
                        <h1 style={{color: "#312450"}}> {this.state.dataHouse.id} Costs </h1>
                    </div>
            
                    <div  style={{marginTop: "3%", marginLeft: "7%"}}>
                        <div style={{position: "relative", marginLeft:"2%", backgroundImage: "linear-gradient(100deg,red,orange)", border: "3px solid white", borderRadius: "10px", width: "200px", height:"150px", textAlign: "center", paddingTop: "5px"}}>
                            <label style= {{zIndex:"1"}}>{this.state.dataHouse.idDivision1}</label>
                            <img src={"images/"+this.state.dataHouse.idDivision1.replace(" ", "_")+".png"} style={{ width: "60px", height: "auto", marginTop:"-15px"}} alt=""/>
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)", zIndex:"1", marginTop:"-5px"}}> {this.state.dataHouse.d1}cents</label>
                        </div>
                        <div style={{position: "absolute", marginLeft: "20%", backgroundImage: "linear-gradient(100deg,rgb(0, 26, 255),rgb(0, 225, 255))", marginTop: "-150px", border: "3px solid white", borderRadius: "10px", width: "200px", height: "150px", textAlign: "center", paddingTop: "5px"}}>
                            <label style= {{zIndex:"1"}}>{this.state.dataHouse.idDivision2}</label>
                            <img src= { "images/"+this.state.dataHouse.idDivision2.replace(" ", "_")+".png" } style={{ width: "60px", height: "auto", marginTop:"-15px"}} alt="" />
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)", marginTop:"-5px"}}> {this.state.dataHouse.d2}cents</label>
                        </div>
                        <div style={{position: "absolute", marginLeft: "38%", backgroundImage: "linear-gradient(100deg, red,pink)", marginTop: "-150px", border: "3px solid white", borderRadius: "10px", width: "200px", height: "150px", textAlign: "center", paddingTop: "5px"}}>
                            <label style= {{zIndex:"1"}}>{this.state.dataHouse.idDivision3}</label>
                            <img src={"images/"+this.state.dataHouse.idDivision3.replace(" ", "_")+".png"} style={{ width: "60px", height: "auto", marginTop:"-15px"}} alt=""/>
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {this.state.dataHouse.d3}cents</label>
                        </div>

                        <div style={{position: "absolute", marginLeft: "56%", backgroundImage: "linear-gradient(100deg, green,rgb(89, 240, 2))", marginTop: "-150px", border: "3px solid white", borderRadius: "10px", width: "200px", height: "150px", textAlign: "center", paddingTop: "5px"}}>
                            <label style= {{zIndex:"1"}}>{this.state.dataHouse.idDivision4}</label>
                            <img src={"images/"+this.state.dataHouse.idDivision4.replace(" ", "_")+".png"} style={{ width: "60px", height: "auto", marginTop:"-15px"}} alt=""/>
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {this.state.dataHouse.d4}cents</label>
                        </div>

                        <div style={{marginTop: "5%"}}>
                            <p style={{position:"absolute", color: "rgb(48, 2, 87)", top:"35%", zIndex:1}}>Choose a day:</p>
                            <i class="fas fa-calendar-alt fa-2x" style={{color: "#312450", position:"absolute", top:"41%", zIndex: 1}}></i>
                            <div style={{position:"absolute", top:"40%", left:"30%", zIndex: 1}}>
                                <DatePicker dateFormat="dd/MM/yyyy" selected={this.state.startDate} style={{}} onChange={date => this.setStartDate(date)}></DatePicker>
                            </div>
                        </div>
                        {this.state.graph.length===0?(
                                <h3></h3>
                            ):(
                                this.state.devices.datasets[0].data = [],
                                
                                this.state.devices.labels = [],

                                this.state.graph.division1.map(
                            
                                    d1 => {
                                        this.state.devices.labels.push(d1.time.split(" ")[1].split(":")[0]+":"+d1.time.split(" ")[1].split(":")[1])
                                        this.state.devices.datasets[1].data.push(d1.value)
                                        this.state.devices.datasets[1].label = d1.name
                                        
                                    }     
                                ),

                                this.state.graph.division2.map(
                            
                                    d2 => {
                                        this.state.devices.datasets[2].data.push(d2.value)
                                        this.state.devices.datasets[2].label = d2.name
                                        
                                    }     
                                ),

                                this.state.graph.division3.map(
                            
                                    d3 => {
                                        this.state.devices.datasets[3].data.push(d3.value)
                                        this.state.devices.datasets[3].label = d3.name
                                        
                                    }     
                                ),

                                this.state.graph.division4.map(
                            
                                    d4 => {
                                        this.state.devices.datasets[0].data.push(d4.value)
                                        this.state.devices.datasets[0].label = d4.name
                                        
                                    }     
                                ),

                                this.state.graph.total.map(
                            
                                    media => {
                                        this.state.devices.datasets[4].data.push(media.value)
                                        
                                    }     
                                ),
                                
                                <div style={{marginTop:"10%", marginLeft:"-50px", width:"98.5%"}}>
                                    <Line data={this.state.devices} />
                                </div>
                                

                            )}
                        
                       
                            
                       
                    </div>
                    
                </div>

                ) }
                            
            </div>
      )
    }
}

 

export default HouseCosts;

