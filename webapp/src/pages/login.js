import React, { Component } from 'react';
import swal from 'sweetalert';
class Login extends Component{
    constructor(){
        super();
        var today = new Date();
    
        this.state = {
            loading : true,
            users : [],
            found: false, 
        }       
    }

    async verifyAccount(){
        console.log("AQUIIIII")
        var size = 0
        var found = false
        fetch("http://192.168.160.103:32080/Login/").then(response=> response.json()).then(data => 
        {
            this.setState({users: data, isLoading:false, });
            data.map(
                user => {
                       if(user.email === document.getElementById("email").value && user.password === document.getElementById("pass").value){
                            console.log("TUDO OK");
                            window.open("/", "_self")
                            found = true
                               
                       }
                       size=size+1 
                },
            )
           
            if(!found){
                swal("Oops!", "Something went wrong!", "error", {button:false})
            }
           
    
        })
        
        
          
    }
  

    render (){
        return(
            <div>
                <div class="limiter">
                    <div class="container-login100">
                        <div class="wrap-login100">
                            <div class="login100-pic js-tilt" data-tilt>
                                <img src={"images/logo.png"}  height="80%" alt="IMG"/>
                            </div>

                            <div class="login100-form validate-form" style={{paddingBottom:"50px"}}>
                                <span class="login100-form-title">
                                    Login
                                </span>

                                <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                                    <input class="input100" id="email" type="text" name="email" placeholder="Email" style={{textAlign: "left",color:"grey"}}/>
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </span>
                                </div>

                                <div class="wrap-input100 validate-input" data-validate = "Password is required">
                                    <input class="input100"  id="pass" type="password" name="pass" placeholder="Password" style={{textAlign: "left",color:"grey"}}/>
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                    </span>
                                </div>
                                
                                <div class="container-login100-form-btn">
                                    <button class="login100-form-btn" onClick = {() => this.verifyAccount()}>
                                        Login
                                    </button>
                                </div>

                                <div class="text-center p-t-136">
                                    <a class="txt2" href="/CreateAccount">
                                        Create your Account
                                        <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                            
            </div>
      )
    }
}

 

export default Login;

