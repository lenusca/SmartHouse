import React, { Component } from 'react';
 import { Redirect } from 'react-router-dom';
 import {
    Link
  } from "react-router-dom";
class CreateAccount extends Component{
    constructor(){
        super();
    
        this.state = {
            loading : true,             
        }       
    }

    async setSave(){
        console.log()
        fetch("http://192.168.160.103:32080/CreateAccount/?email="+document.getElementById("email").value+"&password="+document.getElementById("pass").value)    
    }
  

    render (){
        return(
            <div>
                <div class="limiter" >
                    <div class="container-login100">
                        <div class="wrap-login100">
                            <div class="login100-pic js-tilt" data-tilt>
                                <img src={"images/logo.png"}  height="80%" alt="IMG"/>
                            </div>

                            <div class="login100-form validate-form" style={{paddingBottom:"50px"}}>
                                <span class="login100-form-title">
                                    Create Account
                                </span>

                                <div class="wrap-input100 validate-input" >
                                    <input class="input100" type="text" id="email" name="email" placeholder="Email" style={{textAlign: "left",color:"grey"}}/>
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                    </span>
                                </div>

                                <div class="wrap-input100 validate-input" data-validate = "Password is required">
                                    <input class="input100" type="password" name="pass" placeholder="Password" style={{textAlign: "left",color:"grey"}}/>
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                    </span>
                                </div>

                                <div class="wrap-input100 validate-input" data-validate = "Password is required">
                                    <input class="input100" type="password" id="pass" name="pass" placeholder="Confirm Password" style={{textAlign: "left",color:"grey"}}/>
                                    <span class="focus-input100"></span>
                                    <span class="symbol-input100">
                                        <i class="fa fa-lock" aria-hidden="true"></i>
                                    </span>
                                </div>
                                
                                <div class="container-login100-form-btn">
                                    <button class="login100-form-btn" onClick = {() => this.setSave().then(() => window.open("/", "_self"))}>
                                        Create Account
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                            
            </div>
      )
    }
}

 

export default CreateAccount;

