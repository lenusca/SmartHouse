import React, { Component } from 'react';


class EditDivision extends Component{
    constructor(){
        super();
        this.state = {
            loading : true,
            tops: [],

            
        }

    }
    componentDidMount(){
    
    }
    
  
   
    render (){
        return(
            <div>
                
                <section id="sidebar">
			
				<div id="logo">
					<img src={"images/logo.png"} height="240px" alt=""/> 
				</div>	
				
				<div class="inner">
                        <nav>
                            <ul>	
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}><a href="/">Divisions</a></li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/House">House &#9662;</a>
                                    <ul class="dropdown">
                                        <li ><a href="/House">Sensors Value</a></li>
                                        <li><a href="/HouseCosts">Costs</a></li>
                                    </ul>
                                    
                                </li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/EditSensors">Edit Sensors &#9662;</a>
                                    <ul class="dropdown" style={{marginBottom:"0% !important"}}>
                                        <li><a href="/EditHouseSensors">House</a></li>
                                        <li><a href="/EditDivision">Division</a></li>
                                    </ul>
                                    
                                </li>      
                            </ul>
                            <ul style={{position:"relative", top:"170px", marginBottom:"0%", justifyContent:"center !important", alignContent:"center"}}>
                                <li style={{display: "inline-block"}}><a href="/Login"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                            </ul>
                        </nav>
                </div>
			</section>

            <div id="wrapper" >
                <div style={{marginLeft: "5%", marginTop: "2%", marginBottom: "-20px"}}>
                    <h1 style={{color: "#312450"}}> Select house division </h1>
                </div>
                <div style={{backgroundImage: "url('images/house.png')", minHeight: "852px", backgroundRepeat: "no-repeat", backgroundSize: "85% 100%", backgroundPosition: "center", borderImageWidth: "auto"}}>
                    <button type="button" class="divisionButton" style={{left: "43.5%", top: "55%"}} onClick = {() => {window.location.href='/EditDivisionSensors?idDivision=quarto'}} >BEDROOM</button>
                    <button type="button" class="divisionButton" style={{left: "43.5%", top: "80%"}} onClick = {() => {window.location.href='/EditDivisionSensors?idDivision=sala'}}>LIVING ROOM</button>
                    <button type="button" class="divisionButton" style={{left: "72.5%", top: "55%"}} onClick = {() => {window.location.href='/EditDivisionSensors?idDivision=wc'}} >BATHROOM</button>
                    <button type="button" class="divisionButton" style={{left: "72.5%", top: "80%"}} onClick = {() => {window.location.href='/EditDivisionSensors?idDivision=cozinha'}}>KITCHEN</button>
                    
                </div>
                
            </div>
               
             
            </div>
      )
    }
}

 

export default EditDivision;

