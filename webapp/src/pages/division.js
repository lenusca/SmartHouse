import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { Line } from 'react-chartjs-2';
import { Pie } from 'react-chartjs-2';
import Slider from 'infinite-react-carousel';

class Division extends Component{
    constructor(){
        super();
        var today = new Date();
    
        this.state = {
            loading : true,
            dataDivision: [],
            dataCost: [],
            startDate : today,
            graph: [],
            graphCost: [],
            co2: {labels: [],
             datasets: [
                 {
                
                 label: 'Co2 ppm',
                 backgroundColor: "rgba(246,88,110,1)",
                 borderColor: 'rgba(0,0,0,1)',
                 borderWidth: 1,
                 
            
                 borderRadius: 0,
                 data: []
                 }
             ]},

            temperature: {labels: [],
            datasets: [
                {
                    
                label: 'Temperature ppm',
                backgroundColor: "rgba(246,102,7,1)",
                borderColor: 'rgba(0,0,0,1)',
                borderWidth: 1,
                
            
                borderRadius: 0,
                data: []
                }
            ]},

            humidity: {labels: [],
                datasets: [
                    {
                        
                    label: 'Humidity %',
                    backgroundColor: "rgba(59,143,254,1)",
                    borderColor: 'rgba(0,0,0,1)',
                    borderWidth: 1,
                    
                
                    borderRadius: 0,
                    data: []
                    }
            ]},

            ON : 0,
            OFF : 0, 
            light: {
                labels: [
                    'ON',
                    'OFF',
                ],
                datasets: [{
                    data: [121,121],
                    backgroundColor: [
                        '#F9BE04',
                        '#000000',            
                    ],
                    hoverBackgroundColor: [
                        '#F9BE04',
                        '#000000',
                    ]
                }]
            },
            
            YES: 0,
            NO: 0,
            movement: {
                labels: [
                    'YES',
                    'NO',
                ],
                datasets: [{
                    data: [121,121],
                    backgroundColor: [
                        '#32B701',
                        '#000000',            
                    ],
                    hoverBackgroundColor: [
                        '#32B701',
                        '#000000',
                    ]
                }]
            },

            count: 0,
            
            devices: {
                labels: [],
                datasets: [
                  {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(246,78,8, 0.4)',
                    borderColor: 'rgba(246,78,8, 1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(246,78,8, 1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(246,78,8, 1)',
                    pointHoverBorderColor: 'rgba(246,78,8, 1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  },
                  {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(55,118,255, 0.4)',
                    borderColor: 'rgba(55,118,255, 1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(55,118,255, 1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(55,118,255, 1)',
                    pointHoverBorderColor: 'rgba(55,118,255, 1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  },
                  {
                    label: '',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(246,79,102,0.4)',
                    borderColor: 'rgba(246,79,102,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(246,79,102,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(246,79,102,1)',
                    pointHoverBorderColor: 'rgba(246,79,102,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  },
                  {
                    label: 'Total',
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: 'rgba(49,36,80, 0.4)',
                    borderColor: 'rgba(49,36,80,1)',
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: 'rgba(49,36,80,1)',
                    pointBackgroundColor: '#fff',
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: 'rgba(49,36,80,1)',
                    pointHoverBorderColor: 'rgba(49,36,80,1)',
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: []
                  }

                ]
              },

              show:false,           
        }       
    }

    componentDidMount(){     
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const idDivision = urlParams.get('idDivision');
        console.log(new Date().now);
        fetch("http://192.168.160.103:32080/DivisionInfo/?idDivision="+idDivision).then(response=> response.json()).then(data => this.setState({dataDivision: data, isLoading:false}))
        fetch("http://192.168.160.103:32080/CostDivision/?idDivision="+idDivision).then(response=> response.json()).then(data => this.setState({dataCost: data, isLoading:false}))
    }

    choice(event){
        
        this.setState({
            count : this.state.count + 1,
        });
      
      console.log(this.state.count);
    }

    show(){
        
        console.log(this.state.show);
    }

    setStartDate(startDate){
        var month = startDate.getMonth()+1;
        var day = startDate.getDate();
        
        /*Para colocar 2 digitos*/ 
        if(month.toString().length === 1)
        {
            month = "0"+month; 
        }
        if(day.toString().length ===1){
            day = "0"+day;
        }
        
        var date = startDate.getFullYear()+"/"+month+"/"+day;
        
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const idDivision = urlParams.get('idDivision');
        fetch("http://192.168.160.103:32080/DivisionGraph/?idDivision="+idDivision+"&time="+date).then(response=> response.json()).then(data => this.setState({graph: data, isLoading:false, startDate: startDate}))        
    }

    setStartDate2(startDate){
        var month = startDate.getMonth()+1;
        var day = startDate.getDate();
        
        /*Para colocar 2 digitos*/ 
        if(month.toString().length === 1)
        {
            month = "0"+month; 
        }
        if(day.toString().length ===1){
            day = "0"+day;
        }
        
        var date = startDate.getFullYear()+"/"+month+"/"+day;
        
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const idDivision = urlParams.get('idDivision');
        fetch("http://192.168.160.103:32080/DivisionCostsGraph/?idDivision="+idDivision+"&time="+date).then(response=> response.json()).then(data => this.setState({graphCost: data, isLoading:false, startDate: startDate}))        
    }
  

    render (){
        return(
            <div>
                
                <section id="sidebar">
			
                    <div id="logo">
                        <img src={"images/logo.png"} height="240px" alt=""/> 
                    </div>	
                    
                    <div class="inner">
                        <nav>
                            <ul>	
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}><a href="/">Divisions</a></li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/House">House &#9662;</a>
                                    <ul class="dropdown">
                                        <li ><a href="/House">Sensors Value</a></li>
                                        <li><a href="/HouseCosts">Costs</a></li>
                                    </ul>
                                    
                                </li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/EditSensors">Edit Sensors &#9662;</a>
                                    <ul class="dropdown" style={{marginBottom:"0% !important"}}>
                                        <li><a href="/EditHouseSensors">House</a></li>
                                        <li><a href="/EditDivision">Division</a></li>
                                    </ul>
                                    
                                </li>      
                            </ul>
                            <ul style={{position:"relative", top:"170px", marginBottom:"0%", justifyContent:"center !important", alignContent:"center"}}>
                                <li style={{display: "inline-block"}}><a href="/Login"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                            </ul>
                        </nav>
                </div>
                </section>

                { this.state.dataDivision.length === 0?(
                  
                    <div id="wrapper" style={{backgroundColor:"white", height:"980px"}}>
                        <div style={{marginLeft:"35%", backgroundColor:"white"}}>
                            <iframe src="https://giphy.com/embed/1wrRQfA0DcS7n3zaX0"  height="500px" width="500px" style={{marginTop:"20%"}} title = "TEST"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
                        </div>
                    </div>
                ):(
                    <div id="wrapper" >
                  
                    <div style={{marginLeft: "5%", marginTop: "2%", marginBottom: "-20px"}}>
                        <h1 style={{color: "#312450", position:"absolute"}}> {this.state.dataDivision.idDivision}</h1>
                        <h7 style={{color: "#312450", position:"absolute", top:"4%", left:"80%"}}>Sensors</h7>
                        <div class="switch" style={{marginLeft:"80%", }}>
                            <input type="checkbox" name="toggle" id="toogle" onClick={this.choice.bind(this)}/>
                            <label htmlFor="toggle">
                            <i class="bulb">
                            <span class="bulb-center" ></span>
                            <span class="filament-1" ></span>
                            <span class="filament-2" ></span>
                            <span class="reflections" >
                                <span></span>
                            </span>
                            <span class="sparks">
                                <i class="spark1"></i>
                                <i class="spark2"></i>
                                <i class="spark3"></i>
                                <i class="spark4"></i>
                            </span>
                            </i>    
                            </label>
                        </div> 
                        <h7 style={{color: "#312450", position:"absolute", top:"4%", left:"91%"}}>Costs</h7>
                           
                    </div>
                    {this.state.count % 2 === 0?(
                        
                    <div  style={{marginTop: "8%", marginLeft: "7%"}}>
                        <div style={{position: "relative", marginLeft:"5%", backgroundImage: "linear-gradient(100deg,red,orange)", border: "3px solid white", borderRadius: "10px", width: "150px", height:"100px", textAlign: "center", paddingTop: "10px"}}>
                            <img src={"images/temperature.png"} style={{width: "35px", height: "auto"}} alt=""/>
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {this.state.dataDivision.temperature}ºC</label>
                        </div>
                        <div style={{position: "absolute", marginLeft: "18%", backgroundImage: "linear-gradient(100deg,rgb(0, 26, 255),rgb(0, 225, 255))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                            <img src= {"images/humidity.png"} style={{ width: "35px", height: "auto"}} alt="" />
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {this.state.dataDivision.humidity}%</label>
                        </div>
                        { this.state.dataDivision.light === true?(
                            <div style={{position: "absolute", marginLeft: "31%", backgroundImage: "linear-gradient(100deg,orange,yellow)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                <img src= {"images/lightbulb.png"} style={{width: "35px", height: "auto"}} alt="" />
                                <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> ON </label>
                            </div>
                            
                            
                        ):(
                            <div style={{position: "absolute", marginLeft: "31%", backgroundImage: "linear-gradient(100deg,orange,yellow)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                <img src= {"images/lightbulb.png"} style={{width: "35px", height: "auto"}} alt="" />
                                <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> OFF </label>
                            </div> 
                        )}
                        
                        { this.state.dataDivision.movement === true?(
                            <div style={{position: "absolute", marginLeft: "44%", backgroundImage: "linear-gradient(100deg, green,rgb(89, 240, 2))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                <img src={"images/move.png"} style={{width: "35px", height: "auto"}} alt="" />
                                <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> YES</label>
                            </div>
                        ):(
                            <div style={{position: "absolute", marginLeft: "44%", backgroundImage: "linear-gradient(100deg, green,rgb(89, 240, 2))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                <img src={"images/move.png"} style={{width: "35px", height: "auto"}} alt="" />
                                <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> NO</label>
                            </div>
                        )}
                        <div style={{position: "absolute", marginLeft: "57%", backgroundImage: "linear-gradient(100deg, red,pink)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                            <img src={"images/co2.png"} style={{width: "35px", height: "auto"}} alt=""/>
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {this.state.dataDivision.co2}ppm</label>
                        </div>

                        <div style={{marginTop: "5%"}}>
                            <p style={{position:"absolute", color: "rgb(48, 2, 87)", top:"35%", zIndex:1}}>Choose a day:</p>
                            <i class="fas fa-calendar-alt fa-2x" style={{color: "#312450", position:"absolute", top:"41%", zIndex: 1}}></i>
                            <div style={{position:"absolute", top:"40%", left:"30%", zIndex: 1}}>
                                <DatePicker dateFormat="dd/MM/yyyy" selected={this.state.startDate} style={{}} onChange={date => this.setStartDate(date)}></DatePicker>
                            </div>
                        </div>
                        
                       
                            {this.state.graph.length===0?(
                                 <h3></h3>
                                
                                ):(
                                    this.state.co2.datasets[0].data = [],
                                    this.state.co2.labels = [],
                                    this.state.graph.co2.map(
                                
                                       co2 => {
                                            this.state.co2.labels.push(co2.time.split(" ")[1].split(":")[0]+":"+co2.time.split(" ")[1].split(":")[1])
                                            this.state.co2.datasets[0].data.push(co2.value)
                                            
                                        }     
                                    ),

                                    this.state.temperature.datasets[0].data = [],
                                    this.state.temperature.labels = [],
                                    this.state.graph.temperature.map(
                                
                                       temperature => {
                                            this.state.temperature.labels.push(temperature.time.split(" ")[1].split(":")[0]+":"+temperature.time.split(" ")[1].split(":")[1])
                                            this.state.temperature.datasets[0].data.push(temperature.value)
                                            
                                        }     
                                    ),
                                    
                                    this.state.humidity.datasets[0].data = [],
                                    this.state.humidity.labels = [],
                                    this.state.graph.humidity.map(
                                
                                        humidity => {
                                            this.state.humidity.labels.push(humidity.time.split(" ")[1].split(":")[0]+":"+humidity.time.split(" ")[1].split(":")[1])
                                            this.state.humidity.datasets[0].data.push(humidity.value)
                                            
                                        }     
                                    ),
                                    
                                           
                                    this.state.light.datasets[0].data = [],
                                    this.state.ON = 0,
                                    this.state.OFF = 0,
                                    this.state.graph.light.map(
                                        light => {
                                            if(light.value === true){
                                                this.state.ON += 1
                                                
                                            }
                                            else{
                                                this.state.OFF += 1
                                                
                                            }
                                        },     
                                    ),
                                    this.state.light.datasets[0].data = [this.state.ON, this.state.OFF],

                                    this.state.movement.datasets[0].data = [],
                                    this.state.YES = 0,
                                    this.state.NO = 0,
                                    this.state.graph.movement.map(
                                        movement => {
                                            if(movement.value === true){
                                                this.state.YES += 1
                                                
                                            }
                                            else{
                                                this.state.NO += 1
                                                
                                            }
                                        },     
                                    ),

                                    this.state.movement.datasets[0].data = [this.state.YES, this.state.NO], 
                                    <div style={{position:"relative", marginTop:"200px", marginRight:"60px"}}>
                                        <Slider dots style={{position:"absolute !important", zIndex: 0, top:"40%"}}>
                                        <div style={{ marginTop:"15% !important"}}>
                                            {/* TEMPERATURE */}
                                            {this.state.temperature.length === 0?(
                                                    <div>Loading</div>
                                                ):(
                                                    <Line
                                                        data={this.state.temperature}
                                                        options={{
                                                            title:{
                                                            display:true,
                                                            text:'Temperature ºC',
                                                            fontSize:20
                                                            },
                                                            legend:{
                                                            display:false,
                                                            position:'right'
                                                            },
                                                            scales: {
                                                                xAxes: [{
                                                                    gridLines: {
                                                                        color: "rgba(0, 0, 0, 0)",
                                                                    }
                                                                }],
                                                                yAxes: [{
                                                                    gridLines: {
                                                                        color: "rgba(0, 0, 0, 0)",
                                                                    }
                                                                }]
                                                            },
                                                            backgroundImage:"linear-gradient(100deg, red,pink)",
                                                            spanGaps: true,
                                                            aspectRatio: false,
                                                            
                                                        }
                                                    }
                                                    
                                                    />
                                                    
                                                )}
                                        </div>
                                        <div style={{ marginTop:"15% !important"}}>
                                            {/* HUMIDITY */}
                                            {this.state.humidity.length === 0?(
                                                <div>Loading</div>
                                            ):(
                                                <Line
                                                    data={this.state.humidity}
                                                    options={{
                                                        title:{
                                                        display:true,
                                                        text:'Humidity %',
                                                        fontSize:20
                                                        },
                                                        legend:{
                                                        display:false,
                                                        
                                                        },
                                                        scales: {
                                                            xAxes: [{
                                                                gridLines: {
                                                                    color: "rgba(0, 0, 0, 0)",
                                                                }
                                                            }],
                                                            yAxes: [{
                                                                gridLines: {
                                                                    color: "rgba(0, 0, 0, 0)",
                                                                }
                                                            }]
                                                        },
                                                        backgroundImage:"linear-gradient(100deg, red,pink)",
                                                        spanGaps: true,
                                                        aspectRatio: false,
                                                        
                                                    }
                                                }
                                                
                                                />
                                                
                                            )}         
                                        </div>
                                        <div style={{ marginTop:"15% !important"}}>
                                          {/* LIGHTS */}
                                          {this.state.light.length === 0?(
                                                <div>Loading</div>
                                            ):(
                                                <Pie
                                                    data={this.state.light}
                                                    options={{
                                                        title:{
                                                        display:true,
                                                        text:'Lights',
                                                        fontSize:20
                                                        }, 
                                                    }
                                                }
                                                
                                                />
                                                
                                            )}      
                                        </div>
                                        <div style={{ marginTop:"15% !important"}}>
                                            {/* MOVEMENT */}
                                            {this.state.movement.length === 0?(
                                                <div>Loading</div>
                                            ):(
                                                <Pie
                                                    data={this.state.movement}
                                                    options={{
                                                        title:{
                                                        display:true,
                                                        text:'Movements',
                                                        fontSize:20
                                                        }, 
                                                    }
                                                }
                                                
                                                />
                                            
                                            )}     
                                        </div>
                                        <div style={{ marginTop:"15% !important"}}>
                                           {/* CO2 */}
                                           
                                           {this.state.co2.length === 0?(
                                                <div>Loading</div>
                                            ):(
                                                <Line
                                                    data={this.state.co2}
                                                    options={{
                                                        title:{
                                                        display:true,
                                                        text:'Co2 ppm',
                                                        fontSize:20
                                                        },
                                                        legend:{
                                                        display:false,
                                                        position:'right'
                                                        },
                                                        scales: {
                                                            xAxes: [{
                                                                gridLines: {
                                                                    color: "rgba(0, 0, 0, 0)",
                                                                }
                                                            }],
                                                            yAxes: [{
                                                                gridLines: {
                                                                    color: "rgba(0, 0, 0, 0)",
                                                                }
                                                            }]
                                                        },
                                                        backgroundImage:"linear-gradient(100deg, red,pink)",
                                                        spanGaps: true,
                                                        aspectRatio: false,
                                                        
                                                    }
                                                }
                                                />
                                            )}      
                                        </div>
                                    </Slider>       
                                
                                    </div> 
                                    
                        )}
                     
                       
                    </div>
                    ):(
                        <div  style={{marginTop: "8%", marginLeft: "7%"}}>
                            <div style={{position: "relative", marginLeft:"5%", backgroundImage: "linear-gradient(100deg,red,orange)", border: "3px solid white", borderRadius: "10px", width: "200px", height:"150px", textAlign: "center", paddingTop: "5px"}}>
                                {this.state.dataCost.d1Name === "Eletric Toothbrush"?(
                                    <label style= {{zIndex:"1"}}>Toothbrush</label>
                                ):(
                                    <label style= {{zIndex:"1"}}>{this.state.dataCost.d1Name}</label>
                                )}
                                <img src={"images/"+this.state.dataCost.d1Name.replace(" ", "_")+".png"} style={{ width: "60px", height: "auto", marginTop:"-15px"}} alt=""/>
                                <label style={{fontSize: "20px", color: "rgb(255, 255, 255)", zIndex:"1", marginTop:"-5px"}}> {this.state.dataCost.d1}cents</label>
                            </div>
                            <div style={{position: "absolute", marginLeft: "26.5%", backgroundImage: "linear-gradient(100deg,rgb(0, 26, 255),rgb(0, 225, 255))", marginTop: "-150px", border: "3px solid white", borderRadius: "10px", width: "200px", height: "150px", textAlign: "center", paddingTop: "5px"}}>
                                <label style= {{zIndex:"1"}}>{this.state.dataCost.d2Name}</label>
                                <img src= { "images/"+this.state.dataCost.d2Name.replace(" ", "_")+".png" } style={{ width: "60px", height: "auto", marginTop:"-15px"}} alt="" />
                                <label style={{fontSize: "20px", color: "rgb(255, 255, 255)", marginTop:"-5px"}}> {this.state.dataCost.d2}cents</label>
                            </div>
                            <div style={{position: "absolute", marginLeft: "50%", backgroundImage: "linear-gradient(100deg, red,pink)", marginTop: "-150px", border: "3px solid white", borderRadius: "10px", width: "200px", height: "150px", textAlign: "center", paddingTop: "5px"}}>
                                <label style= {{zIndex:"1"}}>{this.state.dataCost.d3Name}</label>
                                <img src={"images/"+this.state.dataCost.d3Name.replace(" ", "_")+".png"} style={{ width: "60px", height: "auto", marginTop:"-15px"}} alt=""/>
                                <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {this.state.dataCost.d3}cents</label>
                            </div>

                            <div style={{marginTop: "5%"}}>
                                <p style={{position:"absolute", color: "rgb(48, 2, 87)", top:"35%", zIndex:1}}>Choose a day:</p>
                                <i class="fas fa-calendar-alt fa-2x" style={{color: "#312450", position:"absolute", top:"41%", zIndex: 1}}></i>
                                <div style={{position:"absolute", top:"40%", left:"30%", zIndex: 1}}>
                                    <DatePicker dateFormat="dd/MM/yyyy" selected={this.state.startDate} style={{}} onChange={date => this.setStartDate2(date)}></DatePicker>
                                </div>
                            </div>
                            {this.state.graphCost.length===0?(
                                <h3></h3>
                            ):(
                                this.state.devices.datasets[0].data = [],
                                this.state.devices.labels = [],
                                this.state.graphCost.d1.map(
                            
                                    d1 => {
                                        this.state.devices.labels.push(d1.time.split(" ")[1].split(":")[0]+":"+d1.time.split(" ")[1].split(":")[1])
                                        this.state.devices.datasets[0].data.push(d1.value)
                                        this.state.devices.datasets[0].label = this.state.dataCost.d1Name
                                        
                                    }     
                                ),

                                this.state.graphCost.d2.map(
                            
                                    d2 => {
                                        this.state.devices.datasets[1].data.push(d2.value)
                                        this.state.devices.datasets[1].label = this.state.dataCost.d2Name
                                        
                                    }     
                                ),

                                this.state.graphCost.d3.map(
                            
                                    d3 => {
                                        this.state.devices.datasets[2].data.push(d3.value)
                                        this.state.devices.datasets[2].label = this.state.dataCost.d3Name
                                        
                                    }     
                                ),

                                this.state.graphCost.media.map(
                            
                                    media => {
                                        this.state.devices.datasets[3].data.push(media.value)
                                        
                                    }     
                                ),
                                
                                <div style={{marginTop:"10%", marginLeft:"-50px", width:"98.5%"}}>
                                    <Line data={this.state.devices} />
                                </div>
                                

                            )}
                        </div>
                    )}
                    
                </div>

                ) }
                            
            </div>
      )
    }
}

 

export default Division;

