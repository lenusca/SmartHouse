import React, { Component } from 'react';
import "react-datepicker/dist/react-datepicker.css";


class EditHouse extends Component{
    constructor(){
        super();
        var today = new Date();
    
        this.state = {
            loading : true,
            dataHouse: [],
            startDate : today,
            showTemperature: false,
            showMovement: false,
            showLight: false,
            showHumidity: false,
            showC02: false,
            valueTemperature: 0,
            valueHumidity: 0,
            valueCO2: 0,
            valueLight: false,
            countLight: 0,
            valueMovement: false,
            countMovement: 0,
                         
        }       
    }

    componentDidMount(){
        fetch("http://192.168.160.103:32080/HouseInfo/").then(response=> response.json()).then(data => this.setState({dataHouse: data, isLoading:false}))
        this.setState({
            valueTemperature : this.state.dataHouse.temperature,
            valueHumidity : this.state.dataHouse.humidity,
            valueCO2 : this.state.dataHouse.co2,
            valueLight : this.state.dataHouse.light,
            valueMovement : this.state.dataHouse.movement
        })
    }

    handleChange(event){
        this.setState({
            value : event.target.value
        })
    }

    setShowTemperature(){
        this.setState({
            showTemperature: true,
            showMovement: false,
            showLight : false,
            showHumidity: false,
            showC02: false,
        })
    }

    setShowMovement(){
        this.setState({
            showTemperature: false,
            showMovement: true,
            showLight : false,
            showHumidity: false,
            showC02: false,
        })
    }

    setShowLight(){
        this.setState({
            showTemperature: false,
            showMovement: false,
            showLight : true,
            showHumidity: false,
            showC02: false,
        })
    }

    setShowHumidity(){
        this.setState({
            showTemperature: false,
            showMovement: false,
            showLight : false,
            showHumidity: true,
            showC02: false,
        })
    }

    setShowCO2(){
        this.setState({
            showTemperature: false,
            showMovement: false,
            showLight : false,
            showHumidity: false,
            showC02: true,
        })
    }

    changeValueTemperature(){
        this.setState({
            valueTemperature : document.getElementById("myRange").value
        })
        this.state.valueTemperature = document.getElementById("myRange").value
        fetch("http://192.168.160.103:32080/EditSensorsHouse/?idSensor=temperatura"+"&value="+this.state.valueTemperature.toString())   
    }

    changeValueHumidity(){
        this.setState({
            valueHumidity : document.getElementById("myRange").value
        })
        this.state.valueHumidity = document.getElementById("myRange").value
        fetch("http://192.168.160.103:32080/EditSensorsHouse/?idSensor=humidade"+"&value="+this.state.valueHumidity.toString())
    }

    changeValueCO2(){
        this.setState({
            valueCO2 : document.getElementById("myRange").value
        })
        this.state.valueCO2 = document.getElementById("myRange").value
        fetch("http://192.168.160.103:32080/EditSensorsHouse/?idSensor=co2"+"&value="+this.state.valueCO2.toString())
    }

    changeValueLight(event){
        this.setState({
            countLight : this.state.countLight + 1
        })
        if(this.state.countLight % 2 == 0){
            this.state.valueLight = !(this.state.dataHouse.light)
        }
        else{
            this.state.valueLight = this.state.dataHouse.light
        }
        fetch("http://192.168.160.103:32080/EditSensorsHouse/?idSensor=luzes"+"&value="+this.state.valueLight.toString())
    }

    changeValueMovement(event){
        this.setState({
            countMovement : this.state.countMovement + 1
        })
        if(this.state.countMovement % 2 == 0){
            this.state.valueMovement = !(this.state.dataHouse.movement)
        }
        else{
            this.state.valueMovement = this.state.dataHouse.movement
        }
        fetch("http://192.168.160.103:32080/EditSensorsHouse/?idSensor=movimento"+"&value="+this.state.valueMovement.toString())
    }


    render (){
        return(
            <div>
                
                <section id="sidebar">
			
                    <div id="logo">
                        <img src={"images/logo.png"} height="240px" alt=""/> 
                    </div>	
                    
                    <div class="inner">
                        <nav>
                            <ul>	
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}><a href="/">Divisions</a></li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/House">House &#9662;</a>
                                    <ul class="dropdown">
                                        <li ><a href="/House">Sensors Value</a></li>
                                        <li><a href="/HouseCosts">Costs</a></li>
                                    </ul>
                                    
                                </li>
                                <li style={{position:"relative", marginBottom:"0%", justifyContent:"center !important", alignContent:"center", zIndex:"1 !important"}}>
                                    <a href="/EditSensors">Edit Sensors &#9662;</a>
                                    <ul class="dropdown" style={{marginBottom:"0% !important"}}>
                                        <li><a href="/EditHouseSensors">House</a></li>
                                        <li><a href="/EditDivision">Division</a></li>
                                    </ul>
                                    
                                </li>      
                            </ul>
                            <ul style={{position:"relative", top:"170px", marginBottom:"0%", justifyContent:"center !important", alignContent:"center"}}>
                                <li style={{display: "inline-block"}}><a href="/Login"><i class="fas fa-sign-in-alt"></i> Login</a></li>
                            </ul>
                        </nav>
                </div>
                </section>

                { this.state.dataHouse.length === 0?(
                    
                    <div id="wrapper" style={{backgroundColor:"white", height:"980px"}}>
                        <div style={{marginLeft:"35%", backgroundColor:"white"}}>
                            <iframe src="https://giphy.com/embed/1wrRQfA0DcS7n3zaX0"  height="500px" width="500px" style={{marginTop:"20%"}} title = "TEST"  frameBorder="0" class="giphy-embed" allowFullScreen></iframe>
                        </div>
                    </div>
                ):(
                    
                    
                    <div id="wrapper" >
                    <div style={{marginLeft: "5%", marginTop: "2%", marginBottom: "-20px"}}>
                        <h1 style={{color: "#312450"}}> {this.state.dataHouse.idDivision} Sensors </h1>
                    </div>
            
                    <div  style={{marginTop: "5%", marginLeft: "7%"}}>
                     
                        <div onClick={() => this.setShowTemperature()} style={{display:"block", cursor: "pointer", marginLeft:"5%", backgroundImage: "linear-gradient(100deg,red,orange)", border: "3px solid white", borderRadius: "10px", width: "150px", height:"100px", textAlign: "center", paddingTop: "10px"}}>
                            <img src={"images/temperature.png"} style={{width: "35px", height: "auto"}} alt=""/>
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {(this.state.valueTemperature)==undefined?(this.state.dataHouse.temperature):(this.state.valueTemperature)}ºC</label>
                        </div>
                        <div onClick={() => this.setShowHumidity()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "18%", backgroundImage: "linear-gradient(100deg,rgb(0, 26, 255),rgb(0, 225, 255))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                            <img src= {"images/humidity.png"} style={{ width: "35px", height: "auto"}} alt="" />
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {(this.state.valueHumidity)==undefined?(this.state.dataHouse.humidity):(this.state.valueHumidity)}%</label>
                        </div>
                        {this.state.valueLight === undefined?(
                            this.state.dataHouse.light === true?(
                                <div onClick={() => this.setShowLight()} style={{cursor: "pointer", position: "absolute", marginLeft: "31%", backgroundImage: "linear-gradient(100deg,orange,yellow)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src= {"images/lightbulb.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> ON </label>
                                </div>
                               
                            ):(
                                <div onClick={() => this.setShowLight()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "31%", backgroundImage: "linear-gradient(100deg,orange,yellow)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src= {"images/lightbulb.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> OFF </label>
                                </div> 
                              
                                
                            )
                        ):(
                            this.state.valueLight === true?(
                                console.log(this.state.valueLight),
                                <div onClick={() => this.setShowLight()} style={{cursor: "pointer", position: "absolute", marginLeft: "31%", backgroundImage: "linear-gradient(100deg,orange,yellow)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src= {"images/lightbulb.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> ON </label>
                                </div>
                               
                                
                            ):(
                               
                                <div onClick={() => this.setShowLight()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "31%", backgroundImage: "linear-gradient(100deg,orange,yellow)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src= {"images/lightbulb.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> OFF </label>
                                </div> 
                              
                                
                            )
                        )}
                        
                        {this.state.valueMovement === undefined?(
                            
                            this.state.dataHouse.movement === true?(
                                <div onClick={() => this.setShowMovement()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "44%", backgroundImage: "linear-gradient(100deg, green,rgb(89, 240, 2))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src={"images/move.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> YES</label>
                                </div>
                            ):(
                                <div onClick={() => this.setShowMovement()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "44%", backgroundImage: "linear-gradient(100deg, green,rgb(89, 240, 2))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src={"images/move.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> NO</label>
                                </div>
                            )
                        ):(
                            this.state.valueMovement === true?(
                                console.log(this.state.valueMovement),
                                <div onClick={() => this.setShowMovement()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "44%", backgroundImage: "linear-gradient(100deg, green,rgb(89, 240, 2))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src={"images/move.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> YES</label>
                                </div>
                            ):(
                                <div onClick={() => this.setShowMovement()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "44%", backgroundImage: "linear-gradient(100deg, green,rgb(89, 240, 2))", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                                    <img src={"images/move.png"} style={{width: "35px", height: "auto"}} alt="" />
                                    <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> NO</label>
                                </div>
                            )
                        )}
                       
                        <div onClick={() => this.setShowCO2()} style={{display:"block", cursor: "pointer",position: "absolute", marginLeft: "57%", backgroundImage: "linear-gradient(100deg, red,pink)", marginTop: "-100px", border: "3px solid white", borderRadius: "10px", width: "150px", height: "100px", textAlign: "center", paddingTop: "10px"}}>
                            <img src={"images/co2.png"} style={{width: "35px", height: "auto"}} alt=""/>
                            <label style={{fontSize: "20px", color: "rgb(255, 255, 255)"}}> {(this.state.valueCO2)==undefined?(this.state.dataHouse.co2):(this.state.valueCO2)}ppm</label>
                        </div>

                        {this.state.showLight===true?(
                            <div style={{marginLeft: "32%", marginTop: "17%"}}>
                                {this.state.dataHouse.light===true?(
                                    <label class="rocker">
                                        <input type="checkbox" id="checkLight" name="checkLight" onClick={this.changeValueLight.bind(this)} defaultChecked/>
                                        <span class="switch-left" value="true">On</span>
                                        <span class="switch-right" value="false">Off</span>
                                    </label>
                                ):(
                                    <label class="rocker">
                                        <input type="checkbox" id="checkLight" name="checkLight" onClick={this.changeValueLight.bind(this)}/>
                                        <span class="switch-left" value="true">On</span>
                                        <span class="switch-right" value="false">Off</span>
                                    </label>
                                )}
                                 
                            </div>

                        ):(
                            this.state.showMovement===true?(
                                <div style={{marginLeft: "32%", marginTop: "17%"}}>
                                    {this.state.dataHouse.movement===true?(
                                        <label class="rocker">
                                            <input type="checkbox" id="checkMovement" name="checkMovement" onClick={this.changeValueMovement.bind(this)} defaultChecked/>
                                            <span class="switch-left">YES</span>
                                            <span class="switch-right">NO</span>
                                        </label>
                                    ):(
                                        <label class="rocker">
                                            <input type="checkbox" id="checkMovement" name="checkMovement" onClick={this.changeValueMovement.bind(this)} />
                                            <span class="switch-left">YES</span>
                                            <span class="switch-right">NO</span>
                                        </label>
                                    )}
                                 
                            </div>
                            ):(
                                this.state.showTemperature===true?(
                                    <div style={{ marginTop:"-5px"}}>
                                        <img src={"images/thermometer.png"} alt="" style={{marginLeft: "15%", marginTop: "10%"}} />
                                        <div class="slidecontainer" style={{transform: "rotate(270deg)", position: "absolute", top: "-95px", left: "103px"}}>
                                            <input type="range" min="10" max="30" defaultValue={this.state.valueTemperature} class="slider1" id="myRange" onChange={()=> this.changeValueTemperature()} >
                                            </input>
                                        </div>
                                        <h4 style={{position:"absolute", color:"black", top: "450px", left:"1150px"}}>
                                            Max: 30ºC
                                        </h4>
                                        <h3 style={{position:"absolute", color:"black", top: "830px", left:"1150px"}}>
                                            Min: 10ºC
                                        </h3>
                                    </div>
                                ):(
                                    this.state.showHumidity === true?(
                                        <div style={{ marginTop:"-5px"}}>
                                            <img src={"images/drop.png"} alt="" style={{marginLeft: "20%", marginTop: "10%"}} />
                                            <div class="slidecontainer" style={{transform: "rotate(270deg)", position: "absolute", top: "-95px", left: "100px"}}>
                                                <input type="range" min="20" max="90" defaultValue={this.state.valueHumidity} class="slider2" style={{}} id="myRange" onChange={()=> this.changeValueHumidity()} >
                                                </input>
                                            </div>
                                            <h4 style={{position:"absolute", color:"black", top: "450px", left:"1200px"}}>
                                                Max: 90%
                                            </h4>
                                            <h3 style={{position:"absolute", color:"black", top: "830px", left:"1200px"}}>
                                                Min: 20%
                                            </h3>
                                        </div>
                                    ):(
                                        this.state.showC02 === true?(
                                            <div style={{ marginTop:"-5px"}}>
                                                <img src={"images/cloud.png"} alt="" width="800px" height="600px" style={{marginLeft: "20%", marginTop: "5%"}} />
                                                <div class="slidecontainer" style={{position: "absolute", top: "670px", left: "1000px"}}>
                                                    <input type="range" min="100" max="500" defaultValue={this.state.valueCO2} class="slider3" style={{}} id="myRange" onChange={()=> this.changeValueCO2()} >
                                                    </input>
                                                </div>
                                                <h4 style={{position:"absolute", color:"black", top: "670px", left:"1378px"}}>
                                                    Max: 500ppm
                                                </h4>
                                                <h3 style={{position:"absolute", color:"black", top: "670px", left:"845px"}}>
                                                    Min: 100ppm
                                                </h3>
                                            </div>
                                        ):(
                                            <div></div>
                                        )
                                    )
                                )
                            )
                        )}
                        
                       
                            
                     
                       
                    </div>
                    
                </div>

                ) }
                            
            </div>
      )
    }
}

 

export default EditHouse;

