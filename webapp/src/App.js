import React from 'react';
import './App.css';
import Home from './pages/index';
import Division from './pages/division';
import House from './pages/house';
import HouseCosts from './pages/houseCosts';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Login from './pages/login';
import CreateAccount from './pages/createaccount';
import EditHouse from './pages/editHouseSensors';
import EditDivision from './pages/editDivision';
import EditDivisionSensors from './pages/editDivisionSensors';


function App() {
  return (
    <Router>
      <Switch>
        <Route path="/HouseCosts">
          <HouseCosts />
        </Route>
        <Route path="/House">
          <House />
        </Route>
        <Route path="/Division">
          <Division />
        </Route>
        <Route path="/Login">
          <Login />
        </Route>
        <Route path="/CreateAccount">
          <CreateAccount />
        </Route>
        <Redirect from='/CreateAccount' to="/" />
        <Route path="/EditHouseSensors">
          <EditHouse />
        </Route>
        <Route path="/EditDivision">
          <EditDivision/>
        </Route>
        <Route path="/EditDivisionSensors">
          <EditDivisionSensors />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
